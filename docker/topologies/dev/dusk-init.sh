#!/bin/sh

cd $(pwd)

usermod -u $(stat -c "%u" .) www-data 
start-nginx-ci-project
php artisan dusk --colors --debug $@
