#!/bin/bash
COMPOSE_FILE=docker/topologies/dev/docker-compose.yml
export ROOT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
docker-compose -f $COMPOSE_FILE run --rm $@
