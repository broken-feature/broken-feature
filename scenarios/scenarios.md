# Scenarios - Broken Feature

## Register
1. Depuis n'importe où sur le site, si je ne suis pas connecté, je clique sur le bouton <em>Register</em> du header
2. Je me retrouve sur la page d'enregistrement
3. Je renseigne une adresse mail
4. Si l'adresse n'est pas bien formée, une erreur est affichée
5. Je renseigne un nom d'utilisateur
6. Je renseigne un mot de passe
7. Je confirme mon mot de passe
8. Si le mot de passe et la confirmation ne sont pas identiques, une erreur est affichée
9. Je clique sur le bouton <em>Register</em>
10. Si je n'avais aucune erreur dans mes entrées, je suis redirigé sur la page de login

## Login
1. Après m'être enregistré ou après avoir cliqué sur le bouton <em>Login</em> du header, je me retrouve sur la page de login
2. Je renseigne l'adresse e-mail que j'ai donné lorsque je me suis enregistré
3. Je renseigne mon mot de passe
4. Je clique sur le bouton Login
5. Si mes identifiants correspondent à un enregistrement, je suis connecté et suis redirigé vers la page <em>Explore</em>
6. Si mes identifiants ne correspondent pas à un enregistrement, une erreur s'affiche et le champ de mot de passe est vidé

## Explore
1. Après m'être connecté avec succès ou avoir cliqué sur le bouton <em>Explore</em> du header
2. En haut de la page, Je vois un encadré avec une barre de défilement horizontal, contenant des informations sur les quelques projets les mieux notés du site
3. En-dessous, je vois un encadré avec une barre de défilement horizontal, contenant des informations sur les derniers projets mis à jour
4. En-dessous, je vois un encadré avec une barre de défilement horizontal, contenant des informations sur des projets aléatoires du site
5. Je peux cliquer sur le nom de tous ces projets pour arriver sur leurs pages respective

## Project
1. Après avoir cliqué sur un projet depuis n'importe où sur le site, je me retrouve sur sa page
2. Je vois l'image de bannière et l'image de profil du projet
3. Je vois le nom du projet, sa description et le nom du développeur, sur lequel je peux cliquer pour me retrouver sur sa page
4. Si je ne suis pas encore le projet, je peux le suivre au moyen du bouton <em>Follow</em>
5. Si je le suis, je peux arrêter de le suivre en cliquant sur le bouton <em>Followed</em>
6. En-dessous, je vois les différentes mises à jour du projet, triées par ordre d'ancienneté
7. Je peux cliquer sur une mise à jour pour arriver sur sa page
8. Si je suis le créateur du projet, je peux cliquer sur un bouton "+" en-dessous des mises à jour pour en créer une nouvelle, et me faire rediriger vers la page de création de mise à jour

## Update
1. Après avoir cliqué sur une mise à jour spécifique d'un projet, je me retrouve sur la page de cette mise à jour
2. Je vois un descriptif de la mise à jour en haut de la page
3. En-dessous, un résumé des avis généraux est disponible
4. Je vois ensuite une liste des features composant la mise à jour
5. Sur chaque feature, son appréciation est affiché à droite avec une couleur reflétant l'avis général.
6. Si je suis connecté, je peux noter positivement ou négativement une feature au moyen des boutons à gauche de sont titre
7. Si j'ai noté positivement, le triangle vers le haut est coloré en vert. Si j'ai noté négativement, le triangle vers le bas est coloré en rouge
8. Si je clique sur une feature, une description apparaît ainsi qu'un espace commentaire
9. Dans l'espace commentaire, je vois des commentaires écrits par les utilisateurs ainsi que leurs réponses
10. Si je suis connecté, je peux écrire un commentaire sous la feature, répondre à un commentaire ou voter positivement ou négativement un commentaire
11. Si je suis le créateur du projet, je peux ajouter une feature à mon update au moyen du bouton "+" en bas de page

## Profile
1. Si je suis connecté, je peux cliquer sur mon image de profil en haut à gauche du header pour me retrouver sur mon profil. Je peux aussi cliquer sur le nom d'un autre utilisateur lorsqu'il est affiché pour me retrouver sur son profil
2. En arrivant sur la page de profil d'un utilisateur, je vois son image de profil, son nom d'utilisateur ainsi que sa description
3. S'il s'agit de mon profil, je vois aussi mon adresse e-mail
4. S'il s'agit de mon profil, je dispose d'un bouton pour éditer mon profil
   1. En cliquant dessus, la description, le nom d'utilisateur et l'e-mail deviennent des champs de texte
   2. Un bouton <em>Upload picture</em> apparaît en-dessous de l'image de profil. il permet de choisir une image dans son système de fichier local
   3. Je peux cliquer sur le bouton <em>Cancel</em> pour annuler mes changements, ou sur le bouton <em>Update Profile</em> pour valider
5. En milieu de page, je les projets de l'utilisateur, navigables, recherchables et cliquables. De même pour les projets suivis par l'utilisateur
6. S'il s'agit de mon profil, un bouton <em>New Project</em> est présent au-dessus de <em>My Projects</em>. Il mène vers la page de création de projet

## Search results
1. A tout moment sur le site, je peux écrire une chaîne de caractères dans la barre de recherche du header et appuyer sur "Entrée" pour la valider
2. Je me retrouve alors sur la page de résultats de recherche
3. Je vois une liste de projets pour lesquels la chaîne entrée est trouvable dans le nom. Ils sont triés par nombre de followers
4. Je peux cliquer sur le titre du projet ou sur le lien <em>See project</em> pour arriver sur la page du projet
5. Si aucun projet ne correspond, le texte "Aucun projet trouvé" est affiché
