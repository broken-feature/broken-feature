#!/bin/bash

TOPOLOGY=dev
CHROME_DRIVER=86
COMPOSE_FILE=docker/topologies/${TOPOLOGY}/docker-compose.yml
INSTALL=false
UPDATE=false
DETACHED=""
SEED=false

while getopts idus flag
do
    case "${flag}" in
        i) INSTALL=true;;
        u) UPDATE=true;;
        d) DETACHED="-d";;
        s) SEED=true;;
        :) exit;;
        \?) exit;; 
    esac
done


export ROOT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $ROOT_DIR

if [ $INSTALL == true ]
then
    docker-compose -f $COMPOSE_FILE run --rm composer install
    docker-compose -f $COMPOSE_FILE run --rm npm install
    docker-compose -f $COMPOSE_FILE run --rm artisan key:generate
    docker-compose -f $COMPOSE_FILE run --rm artisan storage:link
    docker-compose -f $COMPOSE_FILE run --rm artisan dusk:chrome-driver ${CHROME_DRIVER}
elif [ $UPDATE == true ]
then
    docker-compose -f $COMPOSE_FILE run --rm composer update
    docker-compose -f $COMPOSE_FILE run --rm npm update
fi

if [ $SEED == true ]
then
    if [ "$(docker ps -qf name=${TOPOLOGY}_db_1)" == "" ]
    then
        docker-compose -f $COMPOSE_FILE up -d db
        sleep 10
    fi

    while [ "$(docker logs dev_db_1 | grep -F '[services.d] done')" == "" ]
    do
        echo "Waiting for database availability..." 
        sleep 1
    done 

    docker-compose -f $COMPOSE_FILE run --rm artisan migrate:fresh --seed
fi

docker-compose -f $COMPOSE_FILE up $DETACHED application

