# PDG 2020 - *Broken Feature*

## Membres
- Robin Demarta
- Loïc Dessaules
- Thibaud Franchetti
- Gildas Houlmann

## Terminologie
- **Project**: un project est un logiciel (ou autre création sujette à des améliorations ou mises à jour ponctuelles) qui possèdera des
updates.
- **Update**: une update représente une nouvelle version du project et contient une ou plusieurs features.
- **Feature**: une feature décrit un nouvel élément ajouté/modifié au sein d'une update.
- **Follow**: un utilisateurs peut follow ou suit un ou plusieurs projects afin de signaler son intérêt et être informer des updates futures.
- **Upvote/downvote**: un utilisateur peut upvote ou downvote une feature. Le upvote est un vote positif tandis que le downvote est un vote négatif.

## Description
Le projet Broken Feature a pour but de créer une plateforme web de style réseau sociale, orienté sur la mise à jour de logiciels. Les utilisateurs pourront s'y créer un compte afin de répértorier ensuite leur(s) project(s). Au sein de ces derniers, les auteurs pourront publier et présenter textuellement les updates produites au fur et à mesure du temps.

Chaque update de project sera composée d'une liste de feature(s) que les utilisateurs de la plateforme pourront juger individuellement: un espace de commentaires public ainsi qu'un système de vote (upvote/downvote) permettront à tous les utilisateurs de donner leur avis sur chaque nouveauté du project, à la façon d'un réseau social. Ainsi, un score total d'appréciation pourra être remonté à l'update, affichant son succès (ou échec) global.

## Fonctionnalités
> Les éléments en *italique* sont les fonctionnalités "nice-to-have" hors du MVP. Les autres sont les "must-have" du MVP.

- Création de compte **utilisateurs**
    - Nom d'utilisateur
    - Adresse mail
    - Mot de passe
    - Image de profil
    - Description
    - *Edition du profil*
- Création de **projects**
    - Titre
    - Description
    - Image de fond (bannière)
    - Image de profil
    - *Edition du project*
- Les utilisateurs peuvent follow des projects. Le nombre de followers est un indiquateur de popularité du project affiché sur sa page.
- Ajout d'**updates** dans un project
    - Titre
    - Description
    - Code de version
- Ajout de **features** au sein d'une update
    - Titre
    - Description
- Possibilité d'upvote ou downvote une feature
    - *Plus de possiblités de vote: content, fâché, triste, bug, etc.*
- Ajout de commentaires utilisateurs sur une feature
    - Hiérarchie de réponse
    - Upvote/downvote sur un commentaire
    - Tri des commentaires par date d'ajout ou nombre de vote
- *Espace commentaire sur un update.*
- *Système de badges pour les features. Par ex. une feature avec 500 votes dont 90% positifs sera marquée d'un badge "awesome feature".*
- *Statistiques sur le profil utilisateur*
    - *Score des projects*
    - *Nombre de projects, commentaires, etc.*
- *Possibilité de créer des propositions de nouvelles features par la communauté d'utilisateurs*
    - *Vote par la communauté pour faire resortir les meilleures propositions*
    - *Possibilité pour l'auteur du projet d'ajouter une feature communautaire à l'update d'un project*
- *Système de succès/achievements que les utilisateurs peuvent gagner en atteignant certains objectifs (par ex. "avoir un project à plus de 1'000 followers")*
- *Fil d'actualités des projects que l'utilisateur follow*
- *Notification lors de la sortie d'une update d'un projet suivi*