module.exports = {
    future: {
        // removeDeprecatedGapUtilities: true,
        // purgeLayersByDefault: true,
    },
    purge: [],
    theme: {
        colors: {
            indigo: {
                lighter: '#2D2D45',
                normal: '#1F1E2C',
                dark: '#191823',
                darker: '#121119',
                input: '#424258'
            },
            blue: {
                light: '#EEF6FF',
                accent: '#4895EF'
            },
            white: '#FFF',
            subtitle: '#959595',
            red: '#EA394B',
            orange: '#EE8E36',
            green: '#5DCA50',
            success: '#7FB58A'
        },
        minHeight: {
            '0': '0',
            '1/4': '25%',
            '1/2': '50%',
            '3/4': '75%',
            'full': '100%',
            '10': '10rem',
            '15': '15rem',
            '20': '20rem',
        },
        minWidth: {
            '0': '0',
            '1/4': '25%',
            '1/2': '50%',
            '3/4': '75%',
            '1/5': '20%',
            '2/5': '40%',
            '3/5': '60%',
            '4/5': '80%',
            'full': '100%',
        },
        extend: {},
    },
    variants: {},
    plugins: [],
}
