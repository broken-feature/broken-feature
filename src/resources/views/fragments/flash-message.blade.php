
@if ($message = Session::get('success'))
    <div class="flash-alert bg-success">
        <strong>{{ $message }}</strong>
    </div>
@endif
