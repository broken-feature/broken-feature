<div class="container-box flex items-center mt-4 min-h-full">

    <div class="w-24 mr-2 md:mr-5">
        @include('fragments.logo', ['logo' => $project->logo, 'name' => $project->name, 'link' => route('projects_show', [$project->user->username, $project->slug])])
    </div>

    <div class="w-3/4">
        <a href="{{ route('projects_show', [$project->user->username, $project->slug]) }}">
            <h2 class="text-white font-bold inline mr-2">{{ $project->name }}</h2>
        </a>

        @include('fragments.rating', ['rating' => $project->rating])

        <br>

        <span class="subtitle">
            {{ count($project->followers) }} {{__('pages.profile.followers')}} • {{__('pages.profile.by')}}
            <a href="{{ route('profile_show', $project->user->username)  }}">{{ $project->user->displayName }}</a>
            <span class="opacity-50">{{ '@'.$project->user->username }}</span>
        </span>



        <p class="text-white my-2 text-sm">
            {{ \Illuminate\Support\Str::limit($project->detail, 80, $end='...') }}
        </p>

        <a class="mt-3 text-sm" href="{{ route('projects_show', [$project->user->username, $project->slug]) }}">{{__('pages.profile.see_project')}}</a>
    </div>
</div>
