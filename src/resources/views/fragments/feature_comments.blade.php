@if(empty($feature->comments->all()))
    <p class="subtitle mx-auto">{{ __('pages.update.feature_no_comments') }}</p>
@else
    @include('fragments.feature_comment', ['comment' => $feature->top_comment, 'featureId' => $feature->id, 'currentSubLevel' => 0])
    @for($i = 0; $i < count($feature->root_comments) && $i + 1 != $commentsPerLevel; ++$i)
        @include('fragments.feature_comment', [
            'comment' => $feature->root_comments[$i],
            'featureId' => $feature->id,
            'currentSubLevel' => 0
        ])
    @endfor
@endif
