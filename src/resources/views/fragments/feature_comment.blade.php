<div class="comment">
    <div class="flex items-start">
        <div class="mr-2 md:mr-3">
            @include('fragments.vote', [
                'voteRoute' => 'comments_vote',
                'itemId' => $comment->id,
                'userVote' => $comment->userVote,
                'itemScore' => $comment->voteScore,
            ])
        </div>
        <div>
            <p class="subtitle">
                <a class="text-white link-soft" href="{{ route('profile_show', $comment->user->username) }}">{{ $comment->user->displayName }}</a>
                <span title="{{ $comment->created_at }}">
                    {{ '@' . $comment->user->username . ' • ' . \App\Helpers\DateHelper::getTimeAgo($comment->created_at) }}
                </span>
            </p>
            <p>{{ $comment->text }}</p>
            <span class="link-soft subtitle text-subtitle" onclick="toggleById('f{{ $comment->id }}')"><i class="fas fa-reply"></i> Reply</span>
            @if(!is_null($comment->children) && count($comment->children) > 0)
                <span class="link-soft subtitle text-subtitle ml-3"
                    @if($currentSubLevel == $subLevels)
                        onclick="window.location='{{ route('features_show', [
                            'username' => $update->project->user->username,
                            'projectSlug' => $update->project->slug,
                            'updateId' => $update->id,
                            'featureId' => $f->id]) }}'"
                    @else
                        onclick="toggleById('c{{ $comment->id }}')"
                    @endif
                        title="{{ $comment->child_nodes_count.' '.Str::plural(__('pages.feature.nb_children'), $comment->child_nodes_count) }}">
                    <i class="far fa-comments"></i> {{ count($comment->children).' '.Str::plural(__('pages.feature.nb_responses'), count($comment->children)) }}
                </span>
            @endif
        </div>
    </div>

    <div id="f{{ $comment->id }}" class="hidden ml-2 border-l-2 border-subtitle">
        <div class="comment">
            @include('fragments.comment_form', ['featureId' => $featureId, 'parentId' => $comment->id])
        </div>
    </div>

    {{-- Recursively display comments --}}
    <div id="c{{ $comment->id }}" class="ml-2 border-l-2 border-subtitle">
        @if(!is_null($comment->children) && $currentSubLevel != $subLevels)
            @for($i = 0; $i < count($comment->children) && $i != $commentsPerLevel; ++$i)
                @include('fragments.feature_comment', [
                    'comment' => $comment->children[$i],
                    'commentsPerLevel' => $commentsPerLevel,
                    'currentSubLevel' => $currentSubLevel + 1
                ])
            @endfor
        @endif
    </div>
</div>
