@if(isset($link))
    <a href="{{ $link }}">
@endif
    <div class="logo @isset($class){{ $class }}@endisset
        @if(isset($logo))
            " style="background-image: url('{{ asset('storage/projects/logos/'.$logo) }}')">
        @elseif(isset($name))
            bg-blue-accent">{{\App\Helpers\ProjectHelper::getInitials($name) }}
        @endif
    </div>
@if(isset($link))
</a>
@endif
