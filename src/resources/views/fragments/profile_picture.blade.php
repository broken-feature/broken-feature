<div class="logo @isset($class){{ $class }}@endisset
    @if(isset($logo))
        " style="background-image: url('{{ asset('storage/profiles/'.$logo) }}')">
    @elseif(isset($name))
        bg-blue-accent">{{\App\Helpers\ProjectHelper::getInitials($name) }}
    @endif
</div>
