@extends('layouts.app')

@section('title')
    {{ $update->version.' '.$update->project->name }}
@endsection

@section('content')
    <!-- Presentation banner -->
    <section class="bg-indigo-dark min-h-10 md:min-h-15 lg:min-h-20 bg-cover bg-center flex items-center" @if(isset($update->project->banner))
            style="background-blend-mode: overlay;
            background-image: url('{{ asset('storage/projects/banners/'.$update->project->banner) }}');
            background-color: rgba(18,17,25,0.9);"
        @endisset>

        <div class="container py-8 md:flex">
            <div class="hidden md:block w-2/12 px-4">
                @include('fragments.logo', ['logo' => $update->project->logo, 'name' => $update->project->name])
            </div>
            <div class="container-content m-0 flex items-start flex-col justify-center">
                <h1 class="h1 text-center">{{ $update->version . (empty($update->name) ? '' : ' • '.$update->name) }}</h1>
                <h3 class="h3 text-center">
                    {{ __('pages.update.from_project') }}
                    <a href="{{ route('projects_show', [$update->project->user->username, $update->project->slug])  }}">{{ $update->project->name }}</a>
                </h3>
                <p class="mt-4">
                    {{ $update->detail }}
                </p>
                <p class="subtitle mt-3">{{ __('pages.update.released') . ' ' . $update->release_date }}</p>
            <div class="hidden md:block md:w-2/12"></div>

            </div>
        </div>
    </section>

    <!-- Stats section -->
    <section class="bg-indigo-darker">
        <div class="container">
            <div class="container-content py-8 flex items-center justify-center">
                <div>
                    @include('fragments.rating', ['rating' => $update->rating])
                </div>
                <div class="w-6"></div>
                <div class="subtitle">
                    <p><b>{{ $update->features_count }}</b> {{ Illuminate\Support\Str::plural(__('pages.update.features_count'), $update->features_count) }}</p>
                    <p><b>{{ $update->voters_count }}</b> {{ Illuminate\Support\Str::plural(__('pages.update.votes_count'), $update->voters_count) }}</p>
                    <p><b>{{ $update->comments_count }}</b> {{ Illuminate\Support\Str::plural(__('pages.update.comments_count'), $update->comments_count) }}</p>
                </div>
            </div>
        </div>
    </section>

    <!-- Content section -->
    <section class="container-bg">
        <div class="container">
            <div class="container-content">
                @include('fragments.flash-message')

                @if (\App\Helpers\AuthHelpers::connectedUserIs($update->project->user->username))

                <div class="flex flex-wrap items-start justify-between">
                    <a class="btn mb-4" href="{{ route('features_create', [$update->id]) }}">
                        <i class="fas fa-plus"></i> {{ __('pages.feature_create.title') }}
                    </a>

                    <div class="flex">
                        <a class="btn btn--outline flex-none" href="{{ route('updates_edit', [$update->project->id, $update->id]) }}">
                            <i class="far fa-edit"></i>
                            <span class="hidden md:inline"> {{ __('pages.update.edit_btn') }}</span>
                        </a>
                        <form id="delete-update-form" action="{{ route('updates_destroy', [$update->project->id, $update->id]) }}" method="POST">
                            @csrf
                            @method('delete')
                            <button class="btn btn--outline outline--red ml-6 float-right">
                                <i class="far fa-trash-alt"></i>
                                <span class="hidden md:inline"> {{ __('pages.update.delete_btn') }}</span>
                            </button>
                        </form>
                    </div>
                </div>
                @endif

                <h1 class="h1">{{ __('pages.update.features_title') }}</h1>
                @if($update->features_count == 0)
                    <p class="subtitle">{{ __('pages.update.no_features') }}.</p>
                @else
                    @foreach($update->features as $f)
                        <!-- Feature box -->
                        <div class="container-box mt-8 pt-4">
                            <!-- Feature header -->
                            <div class="item-content">
                                <div class="flex items-center">
                                    <div class="mr-3">
                                        @include('fragments.vote', [
                                            'voteRoute' => 'features_vote',
                                            'itemId' => $f->id,
                                            'userVote' => $f->userVote,
                                            'itemScore' => \App\Helpers\FeatureHelper::calculateVoteScore($f),
                                        ])
                                    </div>
                                    <div>
                                        <h3 class="h3">
                                            <a class="text-white link-soft" href="{{ route('features_show', [
                                            'username' => $update->project->user->username,
                                            'projectSlug' => $update->project->slug,
                                            'updateId' => $update->id,
                                            'featureId' => $f->id]) }}">
                                                {{ $f->name }}
                                            </a>
                                        </h3>
                                        <p class="subtitle">
                                            {{ $f->voters_count.' '.Str::plural(__('pages.update.votes_count'), $f->voters_count).' • ' }}
                                            <span title="{{ count($f->comments).' '.Str::plural(__('pages.update.comments_count'), count($f->comments)) }}">
                                                {{ count($f->comments) }}
                                                <i class="far fa-comment-dots"></i>
                                            </span>
                                        </p>
                                    </div>
                                </div>
                                <div class="self-start">
                                    @include('fragments.rating', ['rating' => \App\Helpers\RatingHelper::calculateRating($f)])
                                </div>
                            </div>
                            <p>{{ $f->detail }}</p>
                            <hr class="border-indigo-normal mt-4">
                            <!-- Comments section -->
                            <div class="item-content flex-col items-stretch my-4 mx-0 md:mx-4 mb-2">
                                <div class="my-4">
                                    @include('fragments.comment_form', ['featureId' => $f->id])
                                </div>
                                @include('fragments.feature_comments', [
                                    'feature' => $f,
                                    'commentsPerLevel' => \App\Http\Controllers\UpdateController::COMMENTS_PER_LEVEL,
                                    'subLevels' => \App\Http\Controllers\UpdateController::COMMENTS_SUBLEVELS
                                ])

                                @if($f->nb_more_comments > 0)
                                    <a class="self-center subtitle link-soft mt-4" href="{{ route('features_show', [
                                    'username' => $update->project->user->username,
                                    'projectSlug' => $update->project->slug,
                                    'updateId' => $update->id,
                                    'featureId' => $f->id]) }}">
                                        {{ $f->nb_more_comments.' '.Str::plural(__('pages.update.feature_more_comments'), $f->nb_more_comments) }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </section>
@endsection

@section('javascript')
    // TODO avoid code duplication with feature page
    const voteForms = document.getElementsByClassName('voteForm');
    const CSRFToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

    for(let form of voteForms) {
        const data = new URLSearchParams(new FormData(form));

        form.onsubmit = (event) => {
            event.preventDefault();

            fetch(form.action, {
                method: form.method,
                body: data,
                headers: {
                    'X-CSRF-TOKEN': CSRFToken
                }
            })
            .then(response => response.json())
            .then(data => {
                // TODO update visuals

                // Update vote arrows and label
                let parent = form.parentElement;
                let upArrow = parent.children[0].children[2];
                let label = parent.children[1];
                let downArrow = parent.children[2].children[2];
                parent.children[1].innerHTML = data.voteScore;

                if(data.userVote === null) { // No vote
                    upArrow.classList.remove('upvoted');
                    downArrow.classList.remove('downvoted');
                    label.classList.remove('upvoted');
                    label.classList.remove('downvoted');
                } else if(data.userVote === 1) { // Upvoted
                    upArrow.classList.add('upvoted');
                    downArrow.classList.remove('downvoted');
                    label.classList.add('upvoted');
                    label.classList.remove('downvoted');
                } else { // Downvoted
                    upArrow.classList.remove('upvoted');
                    downArrow.classList.add('downvoted');
                    label.classList.remove('upvoted');
                    label.classList.add('downvoted');
                }
            })
            .catch(() => window.location.href = "/login");
        }
    }
@endsection
