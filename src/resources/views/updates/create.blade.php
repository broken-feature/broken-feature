@extends('layouts.app')

@section('title')
    {{ __('pages.update_create.title') }}
@endsection

@section('content')
    <section class="container-bg">
        <div class="container mx-auto">

            <h1 class="h1 mb-4 text-center">{{ $projectName }} - {{ __('pages.update_create.title') }}</h1>

            <div class="mt-4 container-box p-10 max-w-xl">

                <form method="POST" action="{{ route('updates_store', [$projectId]) }}">
                    @csrf

                    <!-- Version code -->
                    <div class="flex">
                        <input class="form-input-dark" type="text" name="version" maxlength="15" placeholder="{{ __('forms.placeholders.version') }}" value="{{ old('version') }}" required autofocus>
                        <span class="input__tooltip" aria-label="{{ __('forms.tooltips.version') }}" role="tooltip" data-microtip-position="top-left">
                            <i class="far fa-question-circle subtitle"></i>
                        </span>
                    </div>
                    @error('version')
                        <p class="form__error">{{ $message }}</p>
                    @enderror

                    <!-- Release date -->
                    <input id="flatpickr" class="form-input-dark" type="text" name="releaseDate" placeholder="{{ __('forms.placeholders.release_date') }}" value="{{ old('releaseDate') }}" required>
                    @error('releaseDate')
                        <p class="form__error">{{ $message }}</p>
                    @enderror

                    <!-- Name -->
                    <div class="mt-4 mb-1 subtitle">({{ __('forms.optional') }})</div>
                    <input class="mt-0 form-input-dark" type="text" name="name" maxlength="50" placeholder="{{ __('forms.placeholders.name') }}" value="{{ old('name') }}">
                    @error('name')
                        <p class="form__error">{{ $message }}</p>
                    @enderror

                    <!-- Detail -->
                    <div class="mt-4 mb-1 subtitle">({{ __('forms.optional') }})</div>
                    <textarea class="mt-0 form-input-dark" name="detail" rows="3" maxlength="50000" placeholder="{{ __('forms.placeholders.detail') }}">{{ old('detail') }}</textarea>
                    @error('detail')
                        <p class="form__error">{{ $message }}</p>
                    @enderror

                    <!-- Submit -->
                    <button type="submit" class="mt-10 btn block mx-auto">
                        {{ __('forms.buttons.create') }}
                    </button>

                </form>
            </div>


        </div>
    </section>
@endsection


@section('javascript')
    const fp = flatpickr("#flatpickr", {
        dateFormat: "d-m-Y"
    });
@endsection
