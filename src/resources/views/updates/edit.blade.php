@extends('layouts.app')

@section('title')
    {{ __('pages.update_edit.title') }}
@endsection

@section('content')
    <section class="container-bg">
        <div class="container mx-auto">

            <h1 class="h1 mb-4 text-center">{{ __('pages.update_edit.title') }} "{{ $update->version }}"</h1>

            <div class="mt-4 container-box p-10 max-w-xl">

                <form method="POST" action="{{ route('updates_update', $update->id) }}">
                @csrf
                {{ method_field('PUT') }}

                    <!-- Version -->
                    <div class="flex">
                        <input class="form-input-dark" type="text" name="version" maxlength="15" placeholder="{{ __('forms.placeholders.version') }}" value="{{ old('name', $update->version) }}" autofocus>
                        <span class="input__tooltip" aria-label="{{ __('forms.tooltips.version') }}" role="tooltip" data-microtip-position="top-left">
                            <i class="far fa-question-circle subtitle"></i>
                        </span>
                    </div>
                    @error('name')
                    <p class="form__error">{{ $message }}</p>
                    @enderror

                    <!-- Release date -->
                    <input id="flatpickr" class="form-input-dark" type="text" name="releaseDate" placeholder="{{ __('forms.placeholders.release_date') }}" value="{{ date('d-m-Y', strtotime(old('releaseDate', $update->release_date))) }}" required>
                    @error('releaseDate')
                    <p class="form__error">{{ $message }}</p>
                    @enderror

                    <!-- Name -->
                    <div class="mt-4 mb-1 subtitle">({{ __('forms.optional') }})</div>
                    <input class="form-input-dark mt-0" type="text" name="name" maxlength="50" placeholder="{{ __('forms.placeholders.name') }}" value="{{ old('name', $update->name) }}">
                    @error('name')
                    <p class="form__error">{{ $message }}</p>
                    @enderror

                    <!-- Detail -->
                    <div class="mt-4 mb-1 subtitle">({{ __('forms.optional') }})</div>
                    <textarea class="form-input-dark mt-0" name="detail" rows="3" maxlength="500000" placeholder="{{ __('forms.placeholders.detail') }}">{{ old('detail', $update->detail) }}</textarea>
                    @error('detail')
                    <p class="form__error">{{ $message }}</p>
                    @enderror

                    <!-- Submit -->
                    <button type="submit" class="mt-10 btn block mx-auto">
                        {{ __('forms.buttons.update') }}
                    </button>

                </form>
            </div>


        </div>
    </section>
@endsection


@section('javascript')
    const fp = flatpickr("#flatpickr", {
    dateFormat: "d-m-Y"
    });
@endsection
