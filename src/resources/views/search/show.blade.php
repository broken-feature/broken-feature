@extends('layouts.app')

@section('title')
    {{ __('pages.search.title') }}
@endsection

@section('content')
    <section class="container-bg">
        <div class="container mx-auto">
            <h1 class="h1 mb-4">{{ $results->total().' '.Str::plural(__('pages.search.result_count'),$results->total())}}</h1>

            <div class="container flex flex-row flex-wrap p-0 md:p-10">
                @foreach ($results as $result)
                    <div class="flex-1 flex-shrink-0 min-w-full md:min-w-2/5 min-h-full md:mr-3 md:ml-3 mb-6">
                        @include('fragments.large_project_card', ['project' => $result])
                    </div>
                @endforeach
            </div>

            <div class="pagination-custom">
                {{$results->appends(['q' => request()->q, 'page' => $results->currentPage()])->links()}}
            </div>
        </div>
    </section>
@endsection
