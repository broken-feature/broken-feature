@extends('layouts.app')

@section('title')
    {{ __('pages.project_edit.title') }}
@endsection

@section('content')
    <section class="container-bg">
        <div class="container mx-auto">

            <h1 class="h1 mb-4 text-center">{{ __('pages.project_edit.title') }} "{{ $project->name }}"</h1>

            <div class="mt-4 container-box p-10 max-w-xl">

                <form method="POST" action="{{ route('projects_update', $project->id) }}" enctype="multipart/form-data">
                    @csrf
                    {{ method_field('PUT') }}

                    <!-- Name -->
                    <div class="flex">
                        <input class="form-input-dark" type="text" name="name" placeholder="{{ __('forms.placeholders.name') }}" value="{{ old('name', $project->name) }}" required autofocus>
                        <span class="input__tooltip" aria-label="{{ __('forms.tooltips.project_name') }}" role="tooltip" data-microtip-position="top-left">
                            <i class="far fa-question-circle subtitle"></i>
                        </span>
                    </div>
                    @error('name')
                        <p class="form__error">{{ $message }}</p>
                    @enderror

                    <!-- Detail -->
                    <textarea class="form-input-dark" name="detail" rows="3" maxlength="500000" placeholder="{{ __('forms.placeholders.detail') }}" required>{{ old('detail', $project->detail) }}</textarea>
                    @error('detail')
                        <p class="form__error">{{ $message }}</p>
                    @enderror

                    <!-- Release date -->
                    <input id="flatpickr" class="form-input-dark" type="text" name="releaseDate" placeholder="{{ __('forms.placeholders.release_date') }}" value="{{ date('d-m-Y', strtotime(old('releaseDate', $project->release_date))) }}" required>
                    @error('releaseDate')
                        <p class="form__error">{{ $message }}</p>
                    @enderror

                    <!-- Website -->
                    <div class="mt-4 mb-1 subtitle">({{ __('forms.optional') }})</div>
                    <input class="mt-0 form-input-dark" type="text" name="website" placeholder="{{ __('forms.placeholders.website') }}" value="{{ old('website', $project->website) }}" maxlength="70">
                    @error('website')
                        <p class="form__error">{{ $message }}</p>
                    @enderror

                    <!-- Logo -->
                    <label class="form-label mt-8" for="logoFile">{{ __('forms.placeholders.logo') }} <span class="subtitle">({{ __('forms.optional') }})</span></label>
                    <div class="flex">
                        @if($project->logo != null)
                            <div class="w-16 mr-6 bg-cover" style="background-image: url('{{ asset('storage/projects/logos/'.$project->logo) }}')"></div>
                        @endif
                        <input id="logoFile" class="mt-0 form-input-dark" type="file" name="logoFile" accept="image/png, image/jpeg">
                    </div>
                    @error('logoFile')
                        <p class="form__error">{{ $message }}</p>
                    @enderror
                    @if($project->logo != null)
                        <div class="mt-2">
                            <input id="removeLogo" name="removeLogo" type="checkbox"><label for="removeLogo"><span class="subtitle ml-2">Remove this image </span></label>
                        </div>
                    @endif

                    <!-- banner -->
                    <label class="form-label mt-3" for="bannerFile">{{ __('forms.placeholders.banner') }} <span class="subtitle">({{ __('forms.optional') }})</span></label>
                    <div class="flex">
                        @if($project->banner != null)
                            <div class="w-16 mr-6 bg-cover" style="background-image: url('{{ asset('storage/projects/banners/'.$project->banner) }}')"></div>
                        @endif
                        <input id="bannerFile" class="mt-0 form-input-dark" type="file" name="bannerFile" accept="image/png, image/jpeg">
                    </div>
                    @error('bannerFile')
                        <p class="form__error">{{ $message }}</p>
                    @enderror
                        @if($project->banner != null)
                            <div class="mt-2">
                                <input id="removeBanner" name="removeBanner" type="checkbox"><label for="removeBanner"><span class="subtitle ml-2">Remove this image </span></label>
                            </div>
                    @endif

                    <!-- Submit -->
                    <button type="submit" class="mt-10 btn block mx-auto">
                        {{ __('forms.buttons.update') }}
                    </button>

                </form>
            </div>


        </div>
    </section>
@endsection


@section('javascript')
    const fp = flatpickr("#flatpickr", {
        dateFormat: "d-m-Y"
    });
@endsection
