@extends('layouts.app')

@section('title')
    {{ __('pages.home.title') }}
@endsection

@section('content')

    <!-- Hero section -->
    <section class="bg-indigo-darker">
        <div class="container text-center text-white py-32 px-10">
            <h1 class="text-6xl tracking-widest">
                <span class="font-thin title__double">Broken</span>
                <span class="font-bold uppercase">Feature</span>
            </h1>
            <p class="text-lg font-light opacity-50 tracking-widest">
                The new way to be involved in software updates
            </p>
        </div>
    </section>


    <main class="font-light text-indigo-lighter text-justify md:text-left">

        <!-- Teaser section -->
        <section class="bg-blue-light">
            <div class="container px-10 py-10">
                <p>
                    <span class="font-bold">Gamers</span>, <span class="font-bold">developers</span>, <span class="font-bold">users</span>, <span class="font-bold">publishers</span> or <span class="font-bold">tech enthusiasts</span>, Broken Feature is the place to find them all, the place to bring them all and, in the development, bind them.
                </p>
            </div>
        </section>

        <!-- People section -->
        <section class="bg-white">
            <div class="container px-10 py-10 flex flex-col md:flex-row">
                <article>
                    <h2 class="text-2xl font-bold mb-4">As a software creator</h2>
                    <p>
                        Stay close to your community by getting an instant feedback of your latest changelog.
                        <br><br>
                        Broken Feature is the perfect complement to a traditional issue tracker to reach a wide audience in a new <span class="font-bold">social</span> approach.
                    </p>
                </article>

                <article class="mt-10 md:mt-0 md:ml-20">
                    <h2 class="text-2xl font-bold mb-4">As everyone</h2>
                    <p>
                        Stay up to date with the latest developments of the projects you love, all gathered in a nice social media experience.
                        <br><br>
                        Games, open-source or commercial softwares, they all need <span class="font-bold">your feedback</span> to improve their work and not deliver a broken feature.
                    </p>
                </article>
            </div>
        </section>

        <!-- Functionalities section -->
        <section class="bg-blue-light">
            <div class="container px-10 py-10">
                <div class="flex flex-col md:flex-row">
                    <article class="md:w-1/2 md:text-right">
                        <h2 class="text-2xl font-bold mb-4">Update list</h2>
                        <p>
                            Browse all the projects’ updates and have a quick overview of its popularity.
                        </p>
                    </article>

                    <article class="md:w-1/2 md:ml-10">
                        <img src="{{ asset('images/home/updates.png') }}" alt="Updates">
                    </article>
                </div>

                <div class="flex flex-col-reverse md:flex-row md:mt-10">
                    <article class="md:w-1/2 md:flex md:justify-end">
                        <img src="{{ asset('images/home/features.png') }}" alt="Features">
                    </article>

                    <article class="md:w-1/2 md:ml-10 mt-10 md:mt-0">
                        <h2 class="text-2xl font-bold mb-4">Feature rating</h2>
                        <p>
                            As a project owner, list the brand new features or improvement and let the community express its feedback with a <span class="font-bold">voting</span> and <span class="font-bold">commenting</span> system!
                        </p>
                    </article>
                </div>

            </div>

            <!-- Presentation video -->
            <section class="bg-blue-light">
                <div class="container px-10 py-10 text-center">
                    <video class="m-auto" width="720" height="486" controls>
                        <source src="{{ asset('videos/broken_feature_presentation.mp4') }}" type="video/mp4">
                        Your browser does not support the video tag.
                    </video>
                    <span><i>Only french voice for the moment, english subtitles in progress</i></span>
                </div>
            </section>
        </section>

        <!-- Soon section -->
        <section class="bg-indigo-dark">
            <div class="container  text-center text-white py-48 px-10">
                <p class="text-2xl font-thin">
                    Let's <a class="font-bold" href="{{ route('explore_show') }}">explore</a> some projects.
                </p>
            </div>
        </section>

    </main>
@endsection
