@extends('layouts.app')

@section('title')
    {{ $feature->name }}
@endsection

@section('content')
    <section class="container-bg">
        <div class="container">
            <div class="container-content">
                <!-- Title section -->
                <div class="flex items-center justify-center">
                    <h1 class="h1 text-center mx-4">{{ $feature->name }}</h1>
                    @include('fragments.rating', ['rating' => \App\Helpers\RatingHelper::calculateRating($feature)])
                </div>

                <p class="subtitle text-center">
                    {{ __('pages.feature.of_project')}}
                    <a href="{{ route('projects_show', [$user->username, $project->slug]) }}">{{ $project->name }}</a>
                    {{ __('pages.feature.from_update') }}
                    <a href="{{ route('updates_show', [$user->username, $project->slug, $update->id]) }}">{{ $update->version.' '.$update->name }}</a>
                </p>

                @include('fragments.vote', [
                    'class' => 'vote--lg mt-4',
                    'voteRoute' => 'features_vote',
                    'itemId' => $feature->id,
                    'userVote' => $userVote,
                    'itemScore' => \App\Helpers\FeatureHelper::calculateVoteScore($feature),
                ])
                <p class="subtitle text-center mb-4">{{ $feature->voters_count.' '.Str::plural(__('pages.update.votes_count'), $feature->voters_count) }}</p>

                <!-- Content section -->
                <p class="text-justify">{{ $feature->detail }}</p>

                <hr class="border-indigo-darker my-8">

                <!-- Comments section -->
                <h2 class="h2">
                    <i class="far fa-comment-dots"></i>
                    {{ count($feature->comments).' '.Str::plural(__('pages.feature.nb_comments'), count($feature->comments)) }}
                </h2>
                <div class="item-content flex-col items-stretch">
                    <div class="my-4">
                        @include('fragments.comment_form', ['featureId' => $feature->id])
                    </div>
                    @include('fragments.feature_comments', ['feature' => $feature, 'commentsPerLevel' => -1, 'subLevels' => -1])
                </div>
            </div>
        </div>
    </section>
@endsection

@section('javascript')
    // TODO avoid code duplication with update page
    const voteForms = document.getElementsByClassName('voteForm');
    const CSRFToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

    for(let form of voteForms) {
        const data = new URLSearchParams(new FormData(form));

        form.onsubmit = (event) => {
            event.preventDefault();

            fetch(form.action, {
                method: form.method,
                body: data,
                headers: {
                    'X-CSRF-TOKEN': CSRFToken
                }
            })
            .then(response => response.json())
            .then(data => {
                // TODO update visuals

                // Update vote arrows and label
                let parent = form.parentElement;
                let upArrow = parent.children[0].children[2];
                let label = parent.children[1];
                let downArrow = parent.children[2].children[2];
                parent.children[1].innerHTML = data.voteScore;

                if(data.userVote === null) { // No vote
                    upArrow.classList.remove('upvoted');
                    downArrow.classList.remove('downvoted');
                    label.classList.remove('upvoted');
                    label.classList.remove('downvoted');
                } else if(data.userVote === 1) { // Upvoted
                    upArrow.classList.add('upvoted');
                    downArrow.classList.remove('downvoted');
                    label.classList.add('upvoted');
                    label.classList.remove('downvoted');
                } else { // Downvoted
                    upArrow.classList.remove('upvoted');
                    downArrow.classList.add('downvoted');
                    label.classList.remove('upvoted');
                    label.classList.add('downvoted');
                }
            })
            .catch(() => window.location.href = "/login");
        }
    }
@endsection
