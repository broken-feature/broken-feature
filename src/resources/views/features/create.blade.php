@extends('layouts.app')

@section('title')
    {{ __('pages.feature_create.title') }}
@endsection

@section('content')
    <section class="container-bg">
        <div class="container mx-auto">

            <h1 class="h1 mb-4 text-center">{{ $projectName }} ( {{ $update->version }} ) - {{ __('pages.feature_create.title') }}</h1>

            <div class="mt-4 container-box p-10 max-w-xl">

                <form method="POST" action="{{ route('features_store', [$update->id]) }}">
                    @csrf

                    <!-- Name -->
                    <input class="form-input-dark" type="text" name="name" maxlength="50" placeholder="{{ __('forms.placeholders.name') }}" value="{{ old('name') }}" required autofocus>
                    @error('name')
                     <p class="form__error">{{ $message }}</p>
                    @enderror

                    <!-- Detail -->
                    <textarea class="form-input-dark mb-4" name="detail" rows="4" maxlength="50000" placeholder="{{ __('forms.placeholders.detail') }}" required>{{ old('detail') }}</textarea>
                    @error('detail')
                        <p class="form__error">{{ $message }}</p>
                    @enderror

                    <!-- Note -->
                    <span class="text-red">
                        <i>{{ __('pages.feature_create.note') }}</i>
                    </span>


                    <!-- Submit -->
                    <button type="submit" class="mt-6 btn block mx-auto">
                        {{ __('forms.buttons.create') }}
                    </button>

                </form>
            </div>


        </div>
    </section>
@endsection


@section('javascript')
    const fp = flatpickr("#flatpickr", {
        dateFormat: "d-m-Y"
    });
@endsection
