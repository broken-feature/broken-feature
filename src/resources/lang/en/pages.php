<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pages management (includes header / footer)
    |--------------------------------------------------------------------------
    |
    | All words related to a specific pages
    | <pageName>_title => title of the page
    | ...
    |
    */

    // Header fragment
    'header' => [
        'searchProject' => 'Search a project',
        'logout' => 'Logout',
    ],

    // Footer fragment
    'footer' => [
        'madeWith' => 'Made with',
        'by' => 'by',
    ],

    // Login page
    'login' => [
        'title' => 'Login',
    ],

    // Register page
    'register' => [
        'title' => 'Register',
    ],

    // Home page (landing page on '/')
    'home' => [
        'title' => 'Home',
    ],

    // Explore page
    'explore' => [
        'title' => 'Explore',
        'most_rated_projects' => 'Most rated projects',
        'last_updated_projects' => 'Last updated projects',
        'discover_random_projects' => 'Discover random projects',
        'by'=> 'by',
        'updates'=> 'update',
        'see_project' => 'See project',
    ],

    // Profile page
    'profile' => [
        'title' => 'My profile',
        'alt_img' => 'Profile image',
        'description_title' => 'Bio',
        'update_button_text' => 'Edit profile',
        'followers' => 'followers',
        'by'=> 'by',
        'see_project' => 'See project',
        'my_projects' => 'My projects',
        'their_projects' => 'Projects',
        'no_projects_personal' => 'You don\'t have any projects yet.',
        'no_projects_other_person' => 'This person doesn\'t have any projects yet.',
        'my_follows' => 'Followed projects',
        'no_follows_personal' => 'You don\'t follow any projects yet.',
        'no_follows_other_person' => 'This person doesn\'t follow any projects yet.',
        'delete_btn_text' => 'Delete account',
        'destroy_success_flash' => 'Your account has been successfully deleted',
        'update_success_flash' => 'Your account has been successfully updated',
        'password_update_success_flash' => 'Your password has been successfully changed'
    ],

    'profile_edit' => [
        'title' => 'Edit profile',
        'form_title' => 'Edit your profile'
    ],

    'profile_change_password' => [
        'title' => 'Change password',
        'form_title' => 'Change your password'
    ],

    // Project page
    'project' => [
        'by_user' => 'By',
        'follow_btn' => 'Follow',
        'unfollow_btn' => 'Unfollow',
        'released' => 'Released',
        'no_updates' => 'No updates yet',
        'feature_count' => 'new feature',
        'rating_empty' => 'No rating',
        'delete_btn' => 'Delete project',
        'edit_btn' => 'Edit project',
        'destroy_success_flash' => 'Your project has been successfully deleted.',
        'update_success_flash' => 'Your project has been successfully updated.'
    ],

    'project_create' => [
        'title' => 'New project'
    ],

    'project_edit' => [
        'title' => 'Edit project'
    ],

    // Update page
    'update' => [
        'title' => 'Updates',
        'from_project' => 'Update from',
        'released' => 'Released',
        'features_title' => 'Featuring',
        'no_features' => 'No features yet',
        'features_count' => 'feature',
        'votes_count' => 'vote',
        'comments_count' => 'comment',
        'feature_no_comments' => 'There aren\'t any comments yet.',
        'feature_more_comments' => 'more comment',
        'delete_btn' => 'Delete update',
        'edit_btn' => 'Edit update',
        'destroy_success_flash' => 'Your update has been successfully deleted.',
        'update_success_flash' => 'Your update has been successfully updated.'
    ],

    'update_create' => [
        'title' => "New update"
    ],

    'update_edit' => [
        'title' => 'Edit update'
    ],

    // Feature page
    'feature' => [
        'of_project' => 'Feature of',
        'from_update' => 'added in',
        'nb_comments' => 'comments',
        'insert_comment' => 'Write your comment here.',
        'login_to_comment' => 'Please, login to post a comment.',
        'nb_responses' => 'response',
        'nb_children' => 'child',
    ],

    'feature_create' => [
        'title' => 'New feature',
        'note' => 'Note: a feature will not be editable or deletable, be careful'
    ],

    // Date and time
    'datetime' => [
        'now' => 'just now',
        'second' => 'second',
        'minute' => 'minute',
        'hour' => 'hour',
        'day' => 'day',
        'month' => 'month',
        'year' => 'month',
        'ago' => 'ago',
    ],

    // Search page
    'search' => [
        'title' => 'Search',
        'result_count' => 'result',
    ],

];
