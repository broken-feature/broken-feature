<?php


namespace App\Helpers;


use DateTime;
use Illuminate\Support\Str;

class DateHelper
{
    /**
     * Script from https://www.w3schools.in/php-script/time-ago-function/
     * @param $dateTimeString
     * @return string
     */
    public static function getTimeAgo($dateTimeString) {
        $difference = time() - (new DateTime($dateTimeString))->getTimestamp();

        if( $difference < 1 ) {
            return __('pages.datetime.now');
        }

        $condition = array( 12 * 30 * 24 * 60 * 60 =>  'year',
            30 * 24 * 60 * 60       =>  __('pages.datetime.month'),
            24 * 60 * 60            =>  __('pages.datetime.day'),
            60 * 60                 =>  __('pages.datetime.hour'),
            60                      =>  __('pages.datetime.minute'),
            1                       =>  __('pages.datetime.second')
        );

        foreach( $condition as $secs => $str ) {
            $d = $difference / $secs;

            if( $d >= 1 ) {
                $t = round( $d );
                return $t . ' ' . Str::plural($str, $t) . ' ' . __('pages.datetime.ago');
            }
        }
        return '';
    }
}
