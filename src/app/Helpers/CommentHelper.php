<?php


namespace App\Helpers;


use App\Comment;
use Illuminate\Support\Facades\Auth;

class CommentHelper
{
    /**
     * Recursively create the comment structure of a given feature.
     * @param $featureId
     * @param bool $bestAtTop if true, moves the comment with the best vote-score at index 0 of the returned array
     * @param null $commentId parent comment from which we retrieve the children (if null: we want to start with the roots (1st level) comments only)
     * @return mixed
     */
    public static function createCommentStructure($featureId, $bestAtTop = false, $commentId = null) {
        $comments = Comment::where(['feature_id' => $featureId, 'comment_id' => $commentId])->orderBy('created_at', 'DESC')->get()->all(); // Get all comments as array
        $topCommentIndex = 0;

        for($i = 0; $i < count($comments); ++$i) {
            // Check current user's vote on comment
            if(Auth::check()) {
                $existingVote = Auth::user()->commentVotes()->where('comment_user.comment_id', $comments[$i]->id)->first();
                if(!empty($existingVote)) {
                    $comments[$i]->userVote = $existingVote->pivot->upvote;
                }
            }

            // Set vote score
            $comments[$i]->voteScore = self::calculateVoteScore($comments[$i]);

            // Get children recursively
            $comments[$i]->children = self::createCommentStructure($featureId, false, $comments[$i]->id);

            // Counts ALL the sub-comments
            $comments[$i]->child_nodes_count = 0;
            foreach($comments[$i]->children as $child) {
                $comments[$i]->child_nodes_count += $child->child_nodes_count + 1;
            }

            // Check if best comment yet
            if($bestAtTop && $comments[$i]->voteScore > $comments[$topCommentIndex]->voteScore) {
                $topCommentIndex = $i;
            }
        }

        // Move top comment to first position
        if($bestAtTop && !empty($comments)) {
            $array = $comments;
            array_unshift($array, $comments[$topCommentIndex]); // Push top comment to front
            array_splice($array, $topCommentIndex + 1, 1); // Remove duplicate at old position
            $comments = $array;
        }

        return $comments;
    }

    /**
     * Browse all comment's vote sto add upvotes and subtract downvotes.
     * @param $comment
     * @return int
     */
    public static function calculateVoteScore($comment) {
        $score = 0;
        foreach($comment->votes as $vote) {
            $score += $vote->pivot->upvote == '1' ? 1 : -1;
        }

        return $score;
    }
}
