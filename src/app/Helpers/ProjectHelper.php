<?php


namespace App\Helpers;


class ProjectHelper
{
    public static $NAME_REGEX = '/[A-Za-z0-9]/';
    public static $DATE_REGEX = '/^\d{2}-\d{2}-\d{4}$/';

    /**
     * Shrinks a given string to its slug format:
     * - Converts to lowercase
     * - Removes all that's not alphanumeric char or space
     * - Removes whitespaces at beginning/end of string
     * - Replace all whitespaces (including consecutive ones) by ONE dash
     * @param $name
     * @return string|string[]|null
     */
    public static function slugify($name) {
        // Convert to lowercase
        $name = strtolower($name);

        // Remove all that's not alphanumeric char, space or dash
        $name = preg_replace('/[^a-z0-9 -]/', '', $name);

        // Remove whitespaces at beginning/end of string
        $name = trim($name, "\t\n\r\0\x0B-"); // Default charlist and dashes

        // Replace all whitespaces (including consecutive ones) by ONE dash
        $name = preg_replace('/\s+/', '-', $name);

        return $name;
    }

    /**
     * Returns true if the given value is in valid slug format.
     * @param $name
     * @return bool
     */
    public static function isSlug($name) {
        return self::slugify($name) == $name;
    }

    /**
     * Gives the uppercase first letters of the two first words of a string.
     * Returns one letter if there's only one word.
     * @param $name
     * @return int|string
     */
    public static function getInitials($name) {
        $tokens = explode(' ', $name);
        return strtoupper($tokens[0][0] . (count($tokens) > 1 ? $tokens[1][0] : ''))    ;
    }
}
