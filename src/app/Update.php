<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Update extends Model
{
    /**
     * Update's project.
     */
    public function project() {
        return $this->belongsTo(Project::class);
    }

    /**
     * All the update's features.
     */
    public function features() {
        return $this->hasMany(Feature::class);
    }
}
