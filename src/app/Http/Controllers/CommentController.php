<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Feature;
use App\Helpers\CommentHelper;
use App\Helpers\ProjectHelper;
use App\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class CommentController extends Controller
{
    /**
     * Apply user's vote (upvote or downvote) on a comment.
     * @param $commentId
     * @return \Illuminate\Http\JsonResponse
     */
    public function vote($commentId) {
        // TODO use lockForUpdate() ?

        $existingVote = Auth::user()->commentVotes()->where('comment_user.comment_id', $commentId)->first();
        $commentObj = Comment::findOrFail($commentId);
        // If request param = 0, means downvote, everything else is considered upvote
        $type = request('type') == 0 ? 0 : 1;
        $userVote = $type;

        if(empty($existingVote)) {
            // Create new vote
            Auth::user()->commentVotes()->attach($commentId, ['upvote' => $type]);
        } else if($existingVote->pivot->upvote == $type) {
            // Revoke vote
            Auth::user()->commentVotes()->detach($commentId);
            $userVote = null;
        } else {
            // Change vote type
            Auth::user()->commentVotes()->updateExistingPivot($commentId, ['upvote' => $type]);
        }

        // Redirect to project page
        return response()->json([
            'voteScore' => CommentHelper::calculateVoteScore($commentObj),
            'userVote' => $userVote,
        ]);
    }

    /**
     * Create a new user comment on a feature.
     * @param $featureId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store($featureId){
        $feature = Feature::findOrFail($featureId);
        $update = $feature->updateModel;
        $project = $update->project;

        // Create the form validator
        $validator = Validator::make(request()->all(), [
            'text' => 'required|max:5000'
        ]);

        // Run the validations and return errors with old inputs
        if ($validator->fails()) {
            return redirect()
                ->route('features_show', [$project->user->username, $project->slug, $update->id, $featureId])
                ->withErrors($validator)
                ->withInput();
        }

        // Create and store the new comment

        $comment = new Comment();
        $comment->text = request('text');
        $comment->user()->associate(Auth::user());
        $comment->feature()->associate($feature);
        if(!is_null(request('parentId'))) {
            $comment->comment()->associate(Comment::findOrFail(request('parentId')));
        }

        $comment->save();

        return redirect()->route('features_show', [$project->user->username, $project->slug, $update->id, $featureId]);
    }
}
