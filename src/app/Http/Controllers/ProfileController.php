<?php

namespace App\Http\Controllers;

use App\Helpers\AuthHelpers;
use App\Helpers\ProjectHelper;
use App\Helpers\RatingHelper;
use App\Helpers\WebsiteHelper;
use App\Project;
use App\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{

    private $logosPath = 'public/profiles';

    /**
     * Show my profile page
     */
    public function myProfile(){
        return self::show(Auth::user()->username);
    }

    /**
     * Show the profile page
     */
    public function show($username){
        $user = User::where('username', $username)->firstOrFail();

        $follows = $user->follows()->paginate(10, ['*'], 'follows');
        $ownProjects = $user->projects()->paginate(10, ['*'], 'projects');

        foreach ($follows as $p) {
            $p->rating = RatingHelper::calculateRating($p);
        }

        foreach ($ownProjects as $p) {
            $p->rating = RatingHelper::calculateRating($p);
        }

        return view('profile/show', [
            "user" => $user,
            "follows" => $follows,
            "ownProjects" => $ownProjects,
        ]);
    }

    /**
     * Display the profile edit page
     */
    public function edit(){
        return view('profile/edit', [
            'user' => Auth::user()
        ]);
    }

    /**
     * @param Request $request The http request
     * @return RedirectResponse redirect to view
     */
    public function update(Request $request){
        $user = Auth::user();

        $request->validate([
            'username' => 'required|string|max:15|unique:users,username,'.$user->id.'|regex:'.AuthHelpers::$USERNAME_REGEX,
            'displayName' => 'required|string|max:25',
            'email' => 'required|string|email|max:255|unique:users,email,'.$user->id,
            'bio' => 'string|max:400|nullable',
            'website' => 'string|max:70|nullable',
            'profilePictureFile' => 'mimes:jpg,jpeg,png',
        ]);


        /*** Remove old image management (checkbox checked) ***/
        if($request->get('removeLogo') != null) {
            Storage::delete($this->logosPath . '/' . $user->logo);
            $user->logo = null;
        }

        /*** Update image if needed ***/
        // Store logo file image if exists
        if($request->file('profilePictureFile') != null){
            // First -> delete the old image
            Storage::delete($this->logosPath . '/' . $user->logo);
            // Stored in storage/app/public/projects/logos (https://laravel.com/docs/5.4/filesystem#the-public-disk)
            $logoFilePath = $request->file('profilePictureFile')->store($this->logosPath);
            $exploded = explode('/', $logoFilePath);
            $user->logo = end($exploded);
        }

        /*** Update "normal" users fields ***/
        $user->displayName = $request->get('displayName');
        $user->username = $request->get('username');
        $user->email = $request->get('email');
        $user->bio = $request->get('bio');
        $user->website = WebsiteHelper::removePrefixes($request->get('website'));


        $user->save();

        return redirect()->route('profile_myProfile')->with('success', Lang::get('pages.profile.update_success_flash'));
    }

    /**
     * Display the change password page
     */
    public function changePassword() {
        return view('profile/changePassword', []);
    }
    /**
     * @param Request $request The http request
     * @return RedirectResponse redirect to view
     */
    public function updatePassword(Request $request) {
        $user = Auth::user();

        // Create the form validator
        $validator = Validator::make(request()->all(), [
            'oldPassword' => ['required', 'string'], // Don't ned min xy because this is check at the account creation
            'newPassword' => ['required', 'string', 'min:8', 'confirmed', 'different:oldPassword'],
        ]);

        // Run the validations and return errors and old inputs
        if ($validator->fails()) {
            return redirect()->route('profile_changePassword')->withErrors($validator);
        }

        // After "classic" validation, this is custom validation to check the old password value
        // I can't use after validation hook because of the $request usage
        if(!Hash::check($request->get('oldPassword'), $user->password)){
            $validator->errors()->add('oldPassword', Lang::get('passwords.oldPasswordDoesntMatch'));
            return redirect()->route('profile_changePassword')->withErrors($validator);
        }

        // All is correct -> update the user
        $user->password = Hash::make($request->get('newPassword'));
        $user->save();

        return redirect()->route('profile_myProfile')->with('success', Lang::get('pages.profile.password_update_success_flash'));
    }

    /**
     * Destroy the account
     * @return RedirectResponse Redirect to the login page with flash success message
     */
    public function destroy() {
        $user = Auth::user();
        $user->delete();
        Auth::logout();
        return redirect()->route('login')->with('success', Lang::get('pages.profile.destroy_success_flash'));
    }


}
