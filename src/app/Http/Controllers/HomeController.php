<?php

namespace App\Http\Controllers;

class HomeController extends Controller
{
    /**
     * Show the home page
     */
    public function show()
    {
        return view('home/show');
    }
}
