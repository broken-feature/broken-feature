<?php

namespace App\Http\Controllers;

use App\Feature;
use App\Helpers\AuthHelpers;
use App\Helpers\CommentHelper;
use App\Helpers\FeatureHelper;
use App\Helpers\RatingHelper;
use App\Project;
use App\Update;
use App\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FeatureController extends Controller
{

    public function show($username, $projectSlug, $updateId, $featureId) {

        // Search related project, user and feature
        $user = User::where('username', $username)->firstOrFail();
        $project = $user->projects()->where('slug', $projectSlug)->firstOrFail();
        $update = $project->updates()->findOrFail($updateId);
        $feature = $update->features()->findOrFail($featureId);

        $feature->root_comments = CommentHelper::createCommentStructure($feature->id, true); // Create structure of all the comments

        // Extract top comment
        if(!empty($feature->root_comments)) {
            $feature->top_comment = $feature->root_comments[0];
            $array = $feature->root_comments;
            array_shift($array);
            $feature->root_comments = $array; // Remove first entry
        }

        return view('features/show', [
            'user' => $user,
            'project' => $project,
            'update' => $update,
            'feature' => $feature,
            'userVote' => FeatureHelper::checkUserVote($featureId)
        ]);
    }

    /**
     * Display the create feature view
     * @param int $updateId The update id
     * @return string The view
     */
    public function create($updateId){
        $update = Update::findOrFail($updateId);
        $project = $update->project()->firstOrFail();
        $author = $project->user()->firstOrFail();

        // Check that we are the author of the project for which we want to create an update feature
        if(!AuthHelpers::connectedUserIs($author->username)){
            abort(404);
        }

        return view('features/create', [
            'projectName' => $project->name,
            'update' => $update
        ]);
    }

    /**
     * Validate and store a new feature resource
     * @param int $updateId The update id
     * @param Request $request The http request
     * @return Redirect to view
     */
    public function store(int $updateId, Request $request){
        $update = Update::findOrFail($updateId);
        $project = $update->project()->firstOrFail();
        $author = $project->user()->firstOrFail();

        // Check that we are the author of the project for which we want to create an update feature
        if(!AuthHelpers::connectedUserIs($author->username)){
            abort(404);
        }

        $request->validate([
            'name' => 'required|string|max:50',
            'detail' => 'required|string|max:50000|nullable'
        ]);

        $feature = new Feature();
        $feature->name = $request->get('name');
        $feature->detail = $request->get('detail');
        $feature->updateModel()->associate($update);
        $feature->save();

        // Don't forget to update manually the project updated_at because it concern a project
        // And this way it will appears in first in explore last updated project
        $project->updated_at = Carbon::now();
        $project->save();

        return redirect()->route('features_show', [Auth::user()->username, $project->slug, $update->id, $feature->id]);
    }

    public function vote($featureId) {
        // TODO use lockForUpdate()

        $existingVote = Auth::user()->featureVotes()->where('feature_id', $featureId)->first();
        $featureObj = Feature::findOrFail($featureId);
        // If request param = 0, means downvote, everything else is considered upvote
        $type = request('type') == 0 ? 0 : 1; // TODO do nothing if not 0 nor 1
        $userVote = $type;

        if(empty($existingVote)) {
            // Create new vote
            Auth::user()->featureVotes()->attach($featureId, ['upvote' => $type]);
            $this->changeVoteCounters($featureObj, $type, 1); // Increment upvotes if it is one
        } else if($existingVote->pivot->upvote == $type) {
            // Revoke vote
            Auth::user()->featureVotes()->detach($featureId);
            $this->changeVoteCounters($featureObj, $type == 1 ? -1 : 0, -1); // Decrement upvotes if it was one
            $userVote = null;
        } else {
            // Change vote type
            Auth::user()->featureVotes()->updateExistingPivot($featureId, ['upvote' => $type]);
            $this->changeVoteCounters($featureObj, $type == '1' ? 1 : -1, 0); // Switch increment/decrement
        }

        // Return some page visuals
        $update = $featureObj->updateModel;
        return response()->json([
            'voteScore' => FeatureHelper::calculateVoteScore($featureObj),
            'userVote' => $userVote,
            //'updateVotersCount' => $update->voters_count,
            //'updateRating' => RatingHelper::calculateRating($update),
        ]);
    }

    /**
     * Updates the upvotes_count and voters_count of projects, updates and features tables.
     * @param $feature
     * @param $upvotesAddition
     * @param $votersAddition
     */
    private function changeVoteCounters($feature, $upvotesAddition, $votersAddition) {
        // Limit values to 1
        if($upvotesAddition > 1) $upvotesAddition = 1;
        if($upvotesAddition < -1) $upvotesAddition = -1;
        if($votersAddition > 1) $votersAddition = 1;
        if($votersAddition < -1) $votersAddition = -1;

        // Change feature's vote columns
        $feature->increment('upvotes_count', $upvotesAddition);
        $feature->increment('voters_count', $votersAddition);

        // Change update's vote columns
        $feature->updateModel->increment('upvotes_count', $upvotesAddition);
        $feature->updateModel->increment('voters_count', $votersAddition);

        // Change project's vote columns
        $feature->updateModel->project->increment('upvotes_count', $upvotesAddition);
        $feature->updateModel->project->increment('voters_count', $votersAddition);
    }
}
