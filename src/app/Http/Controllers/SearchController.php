<?php

namespace App\Http\Controllers;

use App\Helpers\RatingHelper;
use App\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller
{

    /**
     * Show the explore view
     */
    public function show() {
        $limit = 8;
        $query = request()->q;

        $results = Project::select("*")
            ->addSelect(DB::raw(RatingHelper::$SQL_RATING_SELECT))
            ->where('name','LIKE','%'.$query.'%');

        $paginatedResults = $results->paginate($limit);

        return view('search/show', [
            'results' => $paginatedResults,
        ]);
    }

}
