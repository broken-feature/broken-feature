<?php

namespace App\Http\Controllers;

use App\Helpers\AuthHelpers;
use App\Helpers\ProjectHelper;
use App\Helpers\RatingHelper;
use App\Helpers\WebsiteHelper;
use App\Project;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\View\View;

class ProjectController extends Controller
{
    // Slugs need to be a class attribute, to use it inside of "$validator->after" method etc.
    private $slug;
    private $oldSlug;

    private $logosPath = '/public/projects/logos';
    private $bannersPath = '/public/projects/banners';

    /**
     * Display the show a project page
     * @param $username string The author username of the project
     * @param $projectSlug string the project slug
     * @return View The show view
     */
    public function show($username, $projectSlug)
    {
        // Find the user and its project or fail (404 error if not found)
        $user = User::where('username', $username)->firstOrFail();
        $project = $user->projects()->where('slug', $projectSlug)->firstOrFail();
        $project->updates = $project->updates()->orderBy('release_date', 'desc')->get(); // Sort updates

        // Add ratings for each update...
        foreach ($project->updates as $u) {
            $u->rating = RatingHelper::calculateRating($u);
        }
        // ...and pour the project
        $project->rating = RatingHelper::calculateRating($project);

        // Check if the current connected user is following the project or not
        $isFollowing = false;
        if(Auth::check() && Auth::user()->follows()->where('project_id', $project->id)->exists()){
            $isFollowing = true;
        }

        return view('projects/show', [
            'project' => $project,
            'isFollowing' => $isFollowing
        ]);
    }

    /**
     * Display the create project view
     */
    public function create(){
        return view('projects/create');
    }

    /**
     * Display the edit project view
     */
    public function edit($projectId){
        $project = Project::where('id', $projectId)->with('user')->firstOrFail();
        // Check that we are the author of the project
        if(!AuthHelpers::connectedUserIs($project['user']['username'])){
            abort(404);
        }

        return view('projects/edit', [
            'project' => $project
        ]);
    }

    /**
     * Validate and store a new project ressource
     * @param Request $request The http request
     * @return Redirect to view
     */
    public function store(Request $request){
        $this->slug = Str::slug($request->get('name'));

        // Create the form validator
        $validator = Validator::make(request()->all(), [
            'name' => 'required|string|max:255|regex:'.ProjectHelper::$NAME_REGEX,
            'detail' => 'required|string|max:50000',
            'releaseDate' => 'required|regex:'.ProjectHelper::$DATE_REGEX,
            'website' => 'string|max:70|nullable',
            'logoFile' => 'mimes:jpg,jpeg,png',
            'bannerFile' => 'mimes:jpg,jpeg,png',
        ]);

        // After normal validations, check CUSTOM validations
        $validator->after(function ($validator) {
            // Project generated slug must be unique for the current user (author)
            if(Project::where(['user_id' => Auth::user()->id, 'slug' => $this->slug])->exists()){
                $validator->errors()->add('name', Lang::get('forms.custom_errors.project_slug'));
            }
        });

        // Run the validations and return errors and old inputs
        if ($validator->fails()) {
            return redirect()->route('projects_create')
                ->withErrors($validator)
                ->withInput();
        }

        // Create and store the project

        $project = new Project();
        // Store logo file image if exists
        if($request->file('logoFile') != null){
            // Stored in storage/app/public/projects/logos (https://laravel.com/docs/5.4/filesystem#the-public-disk)
            $logoFilePath = $request->file('logoFile')->store($this->logosPath);
            $exploded = explode('/', $logoFilePath);
            $project->logo = end($exploded);
        }
        // Store banner file image if exists
        if($request->file('bannerFile') != null){
            // Stored in storage/app/public/projects/banners (https://laravel.com/docs/5.4/filesystem#the-public-disk)
            $bannerFilePath = $request->file('bannerFile')->store($this->bannersPath);
            $exploded = explode('/', $bannerFilePath);
            $project->banner = end($exploded);
        }

        $project->name = $request->get('name');
        $project->slug = $this->slug;
        $project->detail = $request->get('detail');
        $project->website = WebsiteHelper::removePrefixes($request->get('website'));
        // Use Carbon to generate right DB date format
        $project->release_date = Carbon::createFromFormat('d-m-Y', $request->get('releaseDate'))->format('Y-m-d');
        $project->user()->associate(Auth::user());

        $project->save();

        return redirect()->route('projects_show', [Auth::user()->username, $project->slug]);
    }

    /**
     * Validate and update a project
     * @param Request $request The http request
     * @return Redirect to view
     */
    public function update(Request $request, $projectId){
        $project = Project::where('id', $projectId)->with('user')->firstOrFail();
        // Check that we are the author of the project
        if(!AuthHelpers::connectedUserIs($project['user']['username'])){
            abort(404);
        }

        $this->slug = Str::slug($request->get('name'));
        $this->oldSlug = $project->slug;

        // Create the form validator
        $validator = Validator::make(request()->all(), [
            'name' => 'required|string|max:255|regex:'.ProjectHelper::$NAME_REGEX,
            'detail' => 'required|string|max:50000',
            'releaseDate' => 'required|regex:'.ProjectHelper::$DATE_REGEX,
            'website' => 'string|max:70|nullable',
            'logoFile' => 'mimes:jpg,jpeg,png',
            'bannerFile' => 'mimes:jpg,jpeg,png',
        ]);

        // After normal validations, check CUSTOM validations
        $validator->after(function ($validator) {
            // Only generate new slug if the user updated the name
            if($this->slug !== $this->oldSlug) {
                // Project generated slug must be unique for the current user (author)
                if (Project::where(['user_id' => Auth::user()->id, 'slug' => $this->slug])->exists()) {
                    $validator->errors()->add('name', Lang::get('forms.custom_errors.project_slug'));
                }
            }
        });

        // Run the validations and return errors and old inputs
        if ($validator->fails()) {
            return redirect()->route('projects_edit', $projectId)
                ->withErrors($validator)
                ->withInput();
        }

        /*** Remove old image management (checkbox checked) ***/
        if($request->get('removeLogo') != null){
            Storage::delete($this->logosPath . '/' . $project->logo);
            $project->logo = null;
        }
        if($request->get('removeBanner') != null) {
            Storage::delete($this->bannersPath . '/' . $project->banner);
            $project->banner = null;
        }

        /*** Update images if needed ***/
        // Store logo file image if exists
        if($request->file('logoFile') != null){
            // First -> delete the old image
            Storage::delete($this->logosPath . '/' . $project->logo);
            // Stored in storage/app/public/projects/logos (https://laravel.com/docs/5.4/filesystem#the-public-disk)
            $logoFilePath = $request->file('logoFile')->store($this->logosPath);
            $exploded = explode('/', $logoFilePath);
            $project->logo = end($exploded);
        }
        // Store banner file image if exists
        if($request->file('bannerFile') != null){
            // First -> delete the old image
            Storage::delete($this->bannersPath . '/' . $project->banner);
            // Stored in storage/app/public/projects/banners (https://laravel.com/docs/5.4/filesystem#the-public-disk)
            $bannerFilePath = $request->file('bannerFile')->store($this->bannersPath);
            $exploded = explode('/', $bannerFilePath);
            $project->banner = end($exploded);
        }

        /*** Update "normal" project fields ***/
        $project->name = $request->get('name');
        $project->slug = $this->slug !== $this->oldSlug ? $this->slug : $this->oldSlug; // Set oldSlug or new depends on if the user updated it or no
        $project->detail = $request->get('detail');
        $project->website = WebsiteHelper::removePrefixes($request->get('website'));
        // Use Carbon to generate right DB date format
        $project->release_date = Carbon::createFromFormat('d-m-Y', $request->get('releaseDate'))->format('Y-m-d');

        $project->save();

        return redirect()->route('projects_show', [Auth::user()->username, $project->slug])->with('success', Lang::get('pages.project.update_success_flash'));
    }

    /**
     * Follow AND Unfollow management, protected route with auth() middleware, so the user is connected here
     * AJAX functionnality
     * @param $projectId Integer The project id
     * @return \Illuminate\Http\JsonResponse Json response
     */
    public function follow($projectId) {
        $project = Project::where('id', $projectId)->firstOrFail();
        $connectedUser = Auth::user();

        // User is following => we'll unfollow
        if($connectedUser->follows()->where('project_id', $project->id)->exists()){
            $project->followers()->detach($connectedUser);
            $newBtnText = Lang::get('pages.project.follow_btn'); // New text will be follow
        }
        // User is not following => we'll follow
        else {
            $project->followers()->attach($connectedUser);
            $newBtnText = Lang::get('pages.project.unfollow_btn'); // New text will be unfollow
        }

        return response()->json([
            'followersCount' => $project->followers()->count(),
            'newBtnText' => $newBtnText
        ]);
    }

    /**
     * Destroy a project
     * @param $projectId int the project id
     * @return \Illuminate\Http\RedirectResponse Redirect to the profile page with flash success message
     */
    public function destroy($projectId) {
        $project = Project::where('id', $projectId)->with('user')->firstOrFail();
        // Check that we are the author of the project
        if(!AuthHelpers::connectedUserIs($project['user']['username'])){
            abort(404);
        }

        $project->delete();

        return redirect()->route('profile_myProfile')->with('success', Lang::get('pages.project.destroy_success_flash'));
    }
}
