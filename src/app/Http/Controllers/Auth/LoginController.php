<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Override method from AuthenticatesUsers.
     *
     * We define here the login field used to log a user in the website (username field for us)
     * @return string The field used to login a user
     */
    public function username()
    {
        return 'username';
    }

    /**
     * Override method from AuthenticatesUsers.
     *
     * Redirect loggedOut user to the login page
     * @param \Illuminate\Http\Request $request The http request
     * @return \Illuminate\Http\RedirectResponse Redirect to the login page
     */
    public function loggedOut(Request $request)
    {
        return redirect()->route('login');
    }
}
