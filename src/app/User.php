<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'displayName', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * All the user's projects.
     */
    public function projects() {
        return $this->hasMany(Project::class);
    }

    /**
     * Projects that the user follows.
     */
    public function follows() {
        return $this->belongsToMany(Project::class);
    }

    /**
     * ALl user's votes on features.
     */
    public function featureVotes() {
        /**
         * How to access pivot attribute 'vote':
         * 1) use withPivot as shown below to specify which pivot column we need for further querying.
         * 2) $isUpvote = App\User::find(1)->featureVotes[0]->pivot->upvote
         *                ^ Access property 'upvote' of user 1, vote 1
         */
        return $this->belongsToMany(Feature::class)->withPivot('upvote')->withTimestamps();
    }

    /**
     * All the user's projects.
     */
    public function comments() {
        return $this->hasMany(Comment::class);
    }

    /**
     * All user's votes on comments
     */
    public function commentVotes() {
        return $this->belongsToMany(Comment::class)->withPivot('upvote');
    }
}
