<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Update;
use Faker\Generator as Faker;

$factory->define(Update::class, function (Faker $faker) {
    return [
        'name' => $faker->title,
        'detail' => $faker->text(),
        'version' => $faker->randomNumber(4),
        'release_date' => $faker->date(),
    ];
});
