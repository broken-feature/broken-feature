<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Creates the users table.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('email')->unique();
            $table->string('website')->nullable();
            $table->string('username',15)->unique();
            $table->string('displayName',25);
            $table->string('logo')->nullable();
            $table->string('password');
            $table->text('bio')->nullable();
            $table->rememberToken(); // Native Laravel security field
            $table->timestamps();
        });
    }

    /**
     * Drops the users table.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
