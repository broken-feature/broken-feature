<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsTable extends Migration
{
    /**
     * Creates the projects table.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('slug'); // Unique among one user's projects
            $table->text('detail');
            $table->string('website')->nullable();
            $table->string('logo')->nullable();
            $table->string('banner')->nullable();
            $table->date('release_date');
            $table->unsignedInteger('voters_count')->default(0); // All feature votes in this project
            $table->unsignedInteger('upvotes_count')->default(0); // Number of upvotes in this project
            $table->unsignedBigInteger('user_id');
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Drops the projects table.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
