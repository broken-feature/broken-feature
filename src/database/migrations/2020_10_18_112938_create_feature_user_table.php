<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFeatureUserTable extends Migration
{
    /**
     * Creates the feature_user wherein the users' votes on feature are stored.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feature_user', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('feature_id');
            $table->unsignedBigInteger('user_id');
            $table->boolean('upvote');
            $table->timestamps();

            $table->foreign('feature_id')
                ->references('id')->on('features')
                ->onDelete('cascade');

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Drops the feature_user table.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feature_user');
    }
}
