<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'email' => 'robin.demarta@gmail.com',
            'username' => 'rdemarta',
            'displayName' => 'Sikiewz',
            'password' => Hash::make('robin'),
            'bio' => 'Kind human being who likes to create nice applications.',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('users')->insert([
            'email' => 'loic.dessaules@gmail.com',
            'username' => 'ldessaules',
            'displayName' => 'Gollgot',
            'password' => Hash::make('loic'),
            'bio' => 'Kind human being who likes to create nice applications.',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('users')->insert([
            'email' => 'thibaud.franchetti@gmail.com',
            'username' => 'tfranchetti',
            'displayName' => 'Blofeld',
            'password' => Hash::make('thibaud'),
            'bio' => 'Kind human being who likes to create nice applications.',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('users')->insert([
            'email' => 'gildas.houlmann@gmail.com',
            'username' => 'ghoulmann',
            'displayName' => 'Gildou',
            'password' => Hash::make('gildas'),
            'bio' => 'Kind human being who likes to create nice applications.',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('users')->insert([
            'email' => 'mojang@gmail.com',
            'username' => 'Mojang',
            'displayName' => 'Mojang Studio',
            'website' => 'www.minecraft.net',
            'password' => Hash::make('mojang'),
            'bio' => 'We make games.',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('users')->insert([
            'email' => 'jetbrains@gmail.com',
            'username' => 'Jetbrains',
            'displayName' => 'JetbrainsIDEA',
            'website' => 'jetbrains.com',
            'password' => Hash::make('jetbrains'),
            'bio' => 'At JetBrains, code is our passion. Ever since we started, back in 2000, we have strived to make the strongest, most effective developer tools on earth. By automating routine checks and corrections, our tools speed up production, freeing developers to grow, discover and create.',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('users')->insert([
            'email' => 'eugene.roshal@gmail.com',
            'username' => 'eugeneroshal',
            'displayName' => 'Eugene Roshal',
            'password' => Hash::make('eugene'),
            'bio' => 'I am a simple software developper aspiring to become the best in my favorite domain, which is archiving tools.',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
