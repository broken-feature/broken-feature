<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class UpdatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('updates')->insert([
            'name' => 'Village and Pillage',
            'detail' => 'The primary focus of this update is villages, adding a new subset of illagers known as pillagers, vindicators, evokers, and vexes, redesigning village architecture to match the biome where it is located. This update also includes many new blocks and mobs, a revamp of the crafting system by moving some functionalities to different blocks, updates the taiga biome with foxes, and berries, and adds a new bamboo forest jungle variant along with pandas, bamboo and The Texture Update, allowing for a complete retexture of different items in the game.',
            'version' => '1.14',
            'release_date' => '2019-04-23',
            'project_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('updates')->insert([
            'name' => 'Buzzy Bees',
            'detail' => 'added bees and bee-related items such as Beehives, Honey Bottles, and Honeycombs. Alongside these features, this update focused on fixing bugs and improving performance, as well as adding numerous features that were originally exclusive to Bedrock Edition.',
            'version' => '1.15',
            'release_date' => '2019-12-10',
            'project_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('updates')->insert([
            'name' => 'Nether Update',
            'detail' => 'Get to know The dark side of Minecraft. Beyond the Overworld awaits an ancient realm, unexplored by most players—until now. The Nether Update brings the heat to an already fiery dimension with new biomes, mobs, and blocks.',
            'version' => '1.16',
            'release_date' => '2020-06-23',
            'project_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('updates')->insert([
            'name' => '',
            'detail' => 'This update offers some bugfixes and various little improvements based on your feedbacks.',
            'version' => '2020.2.3',
            'release_date' => '2020-10-16',
            'project_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
