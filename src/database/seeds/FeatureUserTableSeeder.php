<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class FeatureUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Users' votes

        DB::table('feature_user')->insert([
            'feature_id' => 1,
            'user_id' => 1,
            'upvote' => true,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('feature_user')->insert([
            'feature_id' => 1,
            'user_id' => 2,
            'upvote' => true,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('feature_user')->insert([
            'feature_id' => 1,
            'user_id' => 3,
            'upvote' => false,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('feature_user')->insert([
            'feature_id' => 1,
            'user_id' => 4,
            'upvote' => true,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('feature_user')->insert([
            'feature_id' => 2,
            'user_id' => 1,
            'upvote' => true,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('feature_user')->insert([
            'feature_id' => 2,
            'user_id' => 2,
            'upvote' => false,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('feature_user')->insert([
            'feature_id' => 3,
            'user_id' => 2,
            'upvote' => true,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        // Store scores in project and update tables

        foreach (App\Project::all() as $p) { // Browse projects
            $projectUpvotes = 0;
            $projectVoters = 0;
            foreach ($p->updates as $u) { // Browse project's updates
                $updateUpvotes = 0;
                $updateVoters = 0;
                foreach ($u->features as $f) { // Browse update's features
                    $featureUpvotes = 0;
                    foreach ($f->votes as $v) { // Check feature's votes
                        if($v->pivot->upvote == 1)
                            ++$featureUpvotes;
                    }

                    $f->update(['voters_count' => count($f->votes)]);
                    $f->update(['upvotes_count' => $featureUpvotes]);

                    $updateVoters += count($f->votes);
                    $updateUpvotes += $featureUpvotes;
                }
                // Store in update table
                $u->update(['upvotes_count' => $updateUpvotes]);
                $u->update(['voters_count' => $updateVoters]);
                $projectUpvotes += $updateUpvotes;
                $projectVoters += $updateVoters;
            }
            // Store in project table
            $p->update(['upvotes_count' => $projectUpvotes]);
            $p->update(['voters_count' => $projectVoters]);
        }
    }
}
