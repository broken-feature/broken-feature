<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class ProjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('projects')->insert([
            'name' => 'Minecraft',
            'slug' => 'minecraft',
            'detail' => 'Welcome to Minecraft. With new games, new updates, and new ways to play, join one of the biggest communities in gaming and start crafting today!',
            'website' => 'www.minecraft.net',
            'logo' => 'minecraft-logo.png',
            'banner' => 'minecraft-banner.jpg',
            'release_date' => '2011-11-18',
            'user_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('projects')->insert([
            'name' => 'PhpStorm',
            'slug' => 'phpstorm',
            'detail' => 'PhpStorm is perfect for working with Symfony, Laravel, Drupal, WordPress, Zend Framework, Magento, Joomla!, CakePHP, Yii, and other frameworks. The editor actually \'gets\' your code and deeply understands its structure, supporting all the PHP language features for modern and legacy projects. It provides the best code completion, refactorings, on-the-fly error prevention, and more.',
            // 'logo' => ''
            // 'banner' => ''
            'release_date' => '2010-08-08',
            'user_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('projects')->insert([
            'name' => 'IntelliJ',
            'slug' => 'intellij',
            'detail' => 'IntelliJ IDEA analyzes your code, looking for connections between symbols across all project files and languages. Using this information it provides indepth coding assistance, quick navigation, clever error analysis, and, of course, refactorings.',
            // 'logo' => ''
            // 'banner' => ''
            'release_date' => '2001-01-01',
            'user_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('projects')->insert([
            'name' => 'GoLand',
            'slug' => 'goland',
            'detail' => 'GoLand is a cross-platform IDE built specially for Go developers',
            // 'logo' => ''
            // 'banner' => ''
            'release_date' => '2018-04-09',
            'user_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('projects')->insert([
            'name' => 'CLion',
            'slug' => 'clion',
            'detail' => 'CLion is a cross-platform IDE built specially for C/C++ developpers who love speed and efficiency',
            // 'logo' => ''
            // 'banner' => ''
            'release_date' => '2012-04-09',
            'user_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('projects')->insert([
            'name' => 'WinRAR',
            'slug' => 'winrar',
            'detail' => 'The best free-but-not-free archiving tool',
            'website' => 'www.winrar.com',
            // 'logo' => ''
            // 'banner' => ''
            'release_date' => '1995-04-22',
            'user_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('projects')->insert([
            'name' => 'Scientific Calculator',
            'slug' => 'scientific-calc',
            'detail' => 'My personal project for the validation of my CFC certificate. Will be maintained if I have time.',
            'website' => 'https://github.com/G-Houlmann/Scientific_Calculator',
            // 'logo' => ''
            // 'banner' => ''
            'release_date' => '2018-06-22',
            'user_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
