<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class FeaturesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('features')->insert([
            'name' => 'Netherite ingot',
            'detail' => 'Netherite items – helmets, chestplates, leggings, boots, swords, pickaxes, axes, shovels and hoes – are tougher, hit harder, and last longer than their diamond equivalents. A diamond sword, for example, will do a total of about 11,000 damage points before it breaks. A netherite one, though, can do more than 16,000 damage. Oh, and it can be enchanted as normal, too.',
            'update_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('features')->insert([
            'name' => 'New mob: Piglins',
            'detail' => 'Piglins can be found all over the Nether, especially around bastion remnants where they gather in large numbers. While Piglins may look harmless, they’re extremely protective of what’s theirs and won’t hesitate to give explorers a good bonk on the noggin if they cross a certain line – something that many players have already learned the hard way in recent Snapshots.',
            'update_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('features')->insert([
            'name' => 'New mob: Bees',
            'detail' => 'Bees are neutral mobs that live in bee nests and beehives. If provoked, bees attack in a swarm to sting the player and inflict poison.',
            'update_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('features')->insert([
            'name' => 'Docker',
            'detail' => 'Now supported: --init command line option.',
            'update_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('features')->insert([
            'name' => 'Version Control',
            'detail' => 'Remember selected options in Git Pull Dialog',
            'update_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
