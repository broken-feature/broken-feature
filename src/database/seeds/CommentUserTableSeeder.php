<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class CommentUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('comment_user')->insert([
            'comment_id' => 1,
            'user_id' => 1,
            'upvote' => true,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('comment_user')->insert([
            'comment_id' => 2,
            'user_id' => 1,
            'upvote' => true,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('comment_user')->insert([
            'comment_id' => 2,
            'user_id' => 2,
            'upvote' => true,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('comment_user')->insert([
            'comment_id' => 2,
            'user_id' => 3,
            'upvote' => true,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('comment_user')->insert([
            'comment_id' => 2,
            'user_id' => 4,
            'upvote' => true,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('comment_user')->insert([
            'comment_id' => 6,
            'user_id' => 1,
            'upvote' => false,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('comment_user')->insert([
            'comment_id' => 6,
            'user_id' => 2,
            'upvote' => false,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('comment_user')->insert([
            'comment_id' => 6,
            'user_id' => 4,
            'upvote' => false,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
