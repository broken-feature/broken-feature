<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database, using all tables' seeders.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UsersTableSeeder::class,
            ProjectsTableSeeder::class,
            UpdatesTableSeeder::class,
            FeaturesTableSeeder::class,
            CommentsTableSeeder::class,
            ProjectUserTableSeeder::class,
            FeatureUserTableSeeder::class,
            CommentUserTableSeeder::class,
        ]);
    }
}
