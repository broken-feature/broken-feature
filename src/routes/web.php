<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Auth management
Auth::routes();

// Home page
Route::get('/', 'HomeController@show')->name('home');

// Profile
Route::get('/u/{username}', 'ProfileController@show')->name('profile_show');

// Explore
Route::get('/explore', 'ExploreController@show')->name('explore_show');

// Project show page
Route::get('/u/{username}/{projectSlug}', 'ProjectController@show')->name('projects_show');

// Update page
Route::get('/u/{username}/{projectSlug}/{updateId}', 'UpdateController@show')->name('updates_show');

// Feature page
Route::get('/u/{username}/{projectSlug}/{updateId}/{featureId}', 'FeatureController@show')->name('features_show');

// Search page
Route::get('/search', 'SearchController@show')->name('search_show');

// Auth routes
Route::middleware(['auth'])->group(function () {

    // Profile
    Route::get('/profile', 'ProfileController@myProfile')->name('profile_myProfile');
    Route::get('/profile/edit', 'ProfileController@edit')->name('profile_edit');
    Route::post('/profile/update', 'ProfileController@update')->name('profile_update');
    Route::get('/profile/change-password', 'ProfileController@changePassword')->name('profile_changePassword');
    Route::post('/profile/change-password', 'ProfileController@updatePassword')->name('profile_updatePassword');
    Route::delete('/profile', 'ProfileController@destroy')->name('profile_destroy');

    // Projects
    Route::get('/projects/create', 'ProjectController@create')->name('projects_create');
    Route::post('/projects', 'ProjectController@store')->name('projects_store');
    Route::post('/projects/{id}/follow', 'ProjectController@follow')->name('projects_follow');
    Route::get('/projects/{id}/edit', 'ProjectController@edit')->name('projects_edit');
    Route::delete('/projects/{id}', 'ProjectController@destroy')->name('projects_destroy');
    Route::put('/projects/{id}', 'ProjectController@update')->name('projects_update');

    // Update page
    Route::get('/projects/{id}/updates/create', 'UpdateController@create')->name('updates_create');
    Route::get('/projects/{projectId}/updates/{updateId}/edit', 'UpdateController@edit')->name('updates_edit');
    Route::post('/projects/{id}/updates', 'UpdateController@store')->name('updates_store');
    Route::delete('/projects/{projectId}/updates/{updateId}', 'UpdateController@destroy')->name('updates_destroy');
    Route::put('/updates/{id}', 'UpdateController@update')->name('updates_update');

    // Feature
    Route::post('/features/{id}/vote', 'FeatureController@vote')->name('features_vote');
    Route::get('/updates/{id}/features/create', 'FeatureController@create')->name('features_create');
    Route::post('/updates/{id}/features', 'FeatureController@store')->name('features_store');

    // Comment
    Route::post('/comments/{id}/vote', 'CommentController@vote')->name('comments_vote');
    Route::post('features/{id}/comments', 'CommentController@store')->name('comments_store');

});
