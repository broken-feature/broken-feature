/**
 * Switch element's visibility from ID.
 * @param elementId
 * @param displayStyle
 */

function toggleById(elementId, displayStyle = 'block') {
    let element = document.getElementById(elementId);

    if(window.getComputedStyle(element).display === 'none') {
        // Show element
        element.style.display = displayStyle;
    } else {
        // Hide element
        element.style.display = 'none';
    }
}

