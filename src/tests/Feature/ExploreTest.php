<?php

namespace Tests\Feature;

use App\Helpers\RatingHelper;
use App\Project;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ExploreTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test the Explore show page
     */
    public function testShow()
    {
        $user = factory(User::class)->create();
        // Create project with 50% rate and most recent update date
        $project50Rate = factory(Project::class)->create([
            'voters_count' => 10,
            'upvotes_count' => 5,
            'updated_at' => Carbon::create(2020, 11, 20),
            'user_id' => $user->id,
        ]);
        // Create project with 90% rate and most middle update date
        $project90Rate = factory(Project::class)->create([
            'voters_count' => 10,
            'upvotes_count' => 9,
            'updated_at' => Carbon::create(2020, 11, 15),
            'user_id' => $user->id,
        ]);
        // Create project with 80% rate and with this older update date
        $project80Rate = factory(Project::class)->create([
            'voters_count' => 10,
            'upvotes_count' => 8,
            'updated_at' => Carbon::create(2020, 11, 10),
            'user_id' => $user->id,
        ]);

        $response = $this->get(route('explore_show'));
        $view = $response->getOriginalContent();

        $response->assertStatus(200);


        // 3 first most rated project must be in the rate order 90% > 80% > 50%

        $this->assertEquals(intval($view['mostRatedProjects'][0]->rating), RatingHelper::calculateRating($project90Rate));
        $this->assertEquals(intval($view['mostRatedProjects'][1]->rating), RatingHelper::calculateRating($project80Rate));
        $this->assertEquals(intval($view['mostRatedProjects'][2]->rating), RatingHelper::calculateRating($project50Rate));

        // Because of the updated_at values we put, must be in this order: 50% > 90% > 80%
        $this->assertEquals(intval($view['lastUpdatedProjects'][0]->rating), RatingHelper::calculateRating($project50Rate));
        $this->assertEquals(intval($view['lastUpdatedProjects'][1]->rating), RatingHelper::calculateRating($project90Rate));
        $this->assertEquals(intval($view['lastUpdatedProjects'][2]->rating), RatingHelper::calculateRating($project80Rate));

        // For random projects, just check the array size
        $this->assertEquals(sizeof($view['randomProjects']), 3);
    }
}
