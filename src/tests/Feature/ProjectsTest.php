<?php

namespace Tests\Feature;

use App\Project;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ProjectsTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test the project store process
     */
    public function testStore()
    {
        $user = factory(User::class)->create();

        $response = $this->actingAs($user)->post(route('projects_store'), [
            'name' => 'MyProject',
            'detail' => 'Detail of the project',
            'release_date' => '17-11-2020',
        ]);

        $response->assertStatus(302);
    }

    /**
     * Test the project show process
     */
    public function testShow()
    {
        $user = factory(User::class)->create();
        $project = factory(Project::class)->create([
            'user_id' => $user->id,
        ]);

        $response = $this->get(route('projects_show', [$user->username, $project->slug]));
        $response->assertStatus(200);
        $response->assertViewHas('project', $project);
    }

    /**
     * Test the follow / unfollow process
     */
    public function testFollow(){
        // Insert simple project into DB
        $user = factory(User::class)->create();
        $project = factory(Project::class)->create([
            'user_id' => $user->id,
        ]);

        // Project must have 0 follower now
        $this->assertEquals(0, sizeof($project->followers));
        // Add one follower (author car follow it's own project), followers count must be 1
        $response = $this->actingAs($user)->post(route('projects_follow', [$project->id]));
        $response->assertJson(["followersCount" => 1]);
        // Remove one follower, followers count must be 0
        $response = $this->actingAs($user)->post(route('projects_follow', [$project->id]));
        $response->assertJson(["followersCount" => 0]);
    }
}
