<?php

namespace Tests\Browser;

use App\Project;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\CreateProjectPage;
use Tests\Browser\Pages\MyProfilePage;
use Tests\Browser\Pages\ProjectPage;
use Tests\DuskTestCase;

class ProjectTest extends DuskTestCase
{

    use DatabaseMigrations;

    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testCreateProject()
    {


        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();
            $browser->loginAs($user->id)
                ->visit('/profile')
                ->on(new MyProfilePage)
                ->click('@newProjectButton')
                ->on(new CreateProjectPage);
            $newProjectName = "My new project";
            $newProjectBio = "A new project I have been working on";
            $newProjectWebsite = "www.brokenfeature.com";
            $newProjectYear = "2020";
            $newProjectMonth = 12;
            $newProjectDay = 12;
            CreateProjectPage::createProject($browser, "$newProjectName", "$newProjectBio", "$newProjectWebsite", "$newProjectYear", $newProjectMonth, $newProjectDay);
            $browser->on(new ProjectPage)
                ->assertSee($newProjectName)
                ->assertSee($newProjectBio)
                ->assertSee($newProjectWebsite)
                ->assertSee($newProjectYear."-".$newProjectMonth."-".$newProjectDay);
        });
    }

    public function testFollowUnfollow(){

        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();

            $browser->loginAs($user->id)
                ->visit('/profile')
                ->on(new MyProfilePage)
                ->assertSeeIn('#myProjectsDiv', __('pages.profile.no_projects_personal'));

            // Create a project
            $project = factory(Project::class)->create([
                'voters_count' => 10,
                'upvotes_count' => 5,
                'updated_at' => Carbon::create(2020, 11, 20),
                'user_id' => $user->id,
            ]);

            //follow and unfollow
            $browser->visit('/u/'.$user->username.'/'.$project->slug)
                ->on(new ProjectPage)
                ->assertSeeIn('@followersCount', "0")
                ->click("@followButton")
                ->pause(1000)
                ->assertSeeIn('@followersCount', "1")
                ->click("@followButton")
                ->pause(1000)
                ->assertSeeIn('@followersCount', "0");
        });
    }
}
