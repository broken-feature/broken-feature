<?php

namespace Tests\Browser;

use App\Project;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\HomePage;
use Tests\Browser\Pages\MyProfilePage;
use Tests\Browser\Pages\SearchResultsPage;
use Tests\DuskTestCase;

class SearchTest extends DuskTestCase
{

    use DatabaseMigrations;

    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testNoResults()
    {

        $this->browse(function (Browser $browser) {

            $browser->visit('/')
                ->on(new HomePage())
                ->type('@searchBar', 'hello')
                ->keys('@searchBar', '{enter}')
                ->on(new SearchResultsPage)
                ->assertDontSee(__('pages.explore.see_project'));

            $project = $this->loginAndCreateProject($browser);

            $notInProjectName = $this->generateRandomString(10);
            while(strpos($notInProjectName, $project->name)){
                $notInProjectName = $this->generateRandomString(10);
            }

            $browser->visit('/')
                ->on(new HomePage)
                ->type('@searchBar', $notInProjectName)
                ->keys('@searchBar', '{enter}')
                ->on(new SearchResultsPage)
                ->assertDontSee('{{__(\'pages.explore.see_project\')}}');
        });
    }

    public function testWithResults()
    {

        $this->browse(function (Browser $browser) {
            $project = $this->loginAndCreateProject($browser);

            $query = $project->name;

            $browser->visit('/')
                ->on(new HomePage)
                ->type('@searchBar', $query)
                ->keys('@searchBar', '{enter}')
                ->on(new SearchResultsPage)
                ->assertSee(__('pages.explore.see_project'))
                ->assertSee($project->name)
                ->assertSee($project->user->username)
                ->assertSee($project->user->displayName);
        });
    }


    private function loginAndCreateProject(Browser $browser){
        $user = factory(User::class)->create();

        $browser->loginAs($user->id)
            ->visit('/profile')
            ->on(new MyProfilePage)
            ->assertSeeIn('#myProjectsDiv', __('pages.profile.no_projects_personal'));

        // Create a project
        return factory(Project::class)->create([
            'voters_count' => 10,
            'upvotes_count' => 5,
            'updated_at' => Carbon::create(2020, 11, 20),
            'user_id' => $user->id,
        ]);
    }

    private function generateRandomString(int $length) {
        return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
    }
}
