<?php

namespace Tests\Browser;

use App\Project;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Str;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\MyProfilePage;
use Tests\Browser\Pages\ProjectPage;
use Tests\DuskTestCase;

class ProfileConsultationTest extends DuskTestCase
{

    use DatabaseMigrations;


    /**
     * Try to see my infos when i am on my profile
     * @throws \Throwable
     */
    public function testSeeMyInfos()
    {
        $user = factory(User::class)->create();

        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user->id)
                ->visit('/profile')
                ->on(new MyProfilePage)
                ->assertSee($user->username)
                ->assertSee($user->email)
                ->assertSee($user->displayName);

            if($user->bio != ""){
                $browser->assertSee($user->bio);
            }
            if($user->website != ""){
                $browser->assertSee($user->website);
            }
        });
    }


    /**
     * Try to create a project and then follow it to see it in my profile under my projects and my follows
     * @throws \Throwable
     */
    public function testSeeMyProjectsAndMyFollows()
    {


        $this->browse(function (Browser $browser) {

            $user = factory(User::class)->create();

            $browser->loginAs($user->id)
                ->visit('/profile')
                ->on(new MyProfilePage)
                ->assertSeeIn('#myProjectsDiv', __('pages.profile.no_projects_personal'));

            // Create a project
            $project = factory(Project::class)->create([
                'voters_count' => 10,
                'upvotes_count' => 5,
                'updated_at' => Carbon::create(2020, 11, 20),
                'user_id' => $user->id,
            ]);

            $browser->visit('/profile')
                ->on(new MyProfilePage)
                ->assertSeeIn('#myProjectsDiv', $project->name)
                ->assertSeeIn('#myProjectsDiv', Str::limit($project->detail, 80, $end='...'))
                ->assertSeeIn('#myFollowsDiv', __('pages.profile.no_follows_personal'))
                ->visit('/u/'.$user->username.'/'.$project->slug)
                ->on(new ProjectPage)
                ->click("@followButton")
                ->click('a[title="My profile"]')
                ->on(new MyProfilePage())
                ->assertSeeIn('#myFollowsDiv', $project->name)
                ->assertSeeIn('#myFollowsDiv', Str::limit($project->detail, 80, $end='...'));

        });
    }
}
