<?php

namespace Tests\Browser\Components;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Component as BaseComponent;

class DatePicker extends BaseComponent
{
    /**
     * Get the root selector for the component.
     *
     * @return string
     */
    public function selector()
    {
        return '.flatpickr-calendar';
    }

    /**
     * Assert that the browser page contains the component.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertVisible($this->selector());
    }

    /**
     * Get the element shortcuts for the component.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@monthSelect' => '.flatpickr-monthDropdown-months',
            '@yearInput' => 'input[type="number"] ',
            '@day1' => '.flatpickr-day[aria-label*=" 1, "]:not(nextMonthDay):not(prevMonthDay) ',
            '@day2' => '.flatpickr-day[aria-label*=" 2, "]:not(nextMonthDay):not(prevMonthDay) ',
            '@day3' => '.flatpickr-day[aria-label*=" 3, "]:not(nextMonthDay):not(prevMonthDay) ',
            '@day4' => '.flatpickr-day[aria-label*=" 4, "]:not(nextMonthDay):not(prevMonthDay) ',
            '@day5' => '.flatpickr-day[aria-label*=" 5, "]:not(nextMonthDay):not(prevMonthDay) ',
            '@day6' => '.flatpickr-day[aria-label*=" 6, "]:not(nextMonthDay):not(prevMonthDay) ',
            '@day7' => '.flatpickr-day[aria-label*=" 7, "]:not(nextMonthDay):not(prevMonthDay) ',
            '@day8' => '.flatpickr-day[aria-label*=" 8, "]:not(nextMonthDay):not(prevMonthDay) ',
            '@day9' => '.flatpickr-day[aria-label*=" 9, "]:not(nextMonthDay):not(prevMonthDay) ',
            '@day10' => '.flatpickr-day[aria-label*=" 10, "]:not(nextMonthDay):not(prevMonthDay) ',
            '@day11' => '.flatpickr-day[aria-label*=" 11, "]:not(nextMonthDay):not(prevMonthDay) ',
            '@day12' => '.flatpickr-day[aria-label*=" 12, "]:not(nextMonthDay):not(prevMonthDay) ',
            '@day13' => '.flatpickr-day[aria-label*=" 13, "]:not(nextMonthDay):not(prevMonthDay) ',
            '@day14' => '.flatpickr-day[aria-label*=" 14, "]:not(nextMonthDay):not(prevMonthDay) ',
            '@day15' => '.flatpickr-day[aria-label*=" 15, "]:not(nextMonthDay):not(prevMonthDay) ',
            '@day16' => '.flatpickr-day[aria-label*=" 16, "]:not(nextMonthDay):not(prevMonthDay) ',
            '@day17' => '.flatpickr-day[aria-label*=" 17, "]:not(nextMonthDay):not(prevMonthDay) ',
            '@day18' => '.flatpickr-day[aria-label*=" 18, "]:not(nextMonthDay):not(prevMonthDay) ',
            '@day19' => '.flatpickr-day[aria-label*=" 19, "]:not(nextMonthDay):not(prevMonthDay) ',
            '@day20' => '.flatpickr-day[aria-label*=" 20, "]:not(nextMonthDay):not(prevMonthDay) ',
            '@day21' => '.flatpickr-day[aria-label*=" 21, "]:not(nextMonthDay):not(prevMonthDay) ',
            '@day22' => '.flatpickr-day[aria-label*=" 22, "]:not(nextMonthDay):not(prevMonthDay) ',
            '@day23' => '.flatpickr-day[aria-label*=" 23, "]:not(nextMonthDay):not(prevMonthDay) ',
            '@day24' => '.flatpickr-day[aria-label*=" 24, "]:not(nextMonthDay):not(prevMonthDay) ',
            '@day25' => '.flatpickr-day[aria-label*=" 25, "]:not(nextMonthDay):not(prevMonthDay) ',
            '@day26' => '.flatpickr-day[aria-label*=" 26, "]:not(nextMonthDay):not(prevMonthDay) ',
            '@day27' => '.flatpickr-day[aria-label*=" 27, "]:not(nextMonthDay):not(prevMonthDay) ',
            '@day28' => '.flatpickr-day[aria-label*=" 28, "]:not(nextMonthDay):not(prevMonthDay) ',
            '@day29' => '.flatpickr-day[aria-label*=" 29, "]:not(nextMonthDay):not(prevMonthDay) ',
            '@day30' => '.flatpickr-day[aria-label*=" 30, "]:not(nextMonthDay):not(prevMonthDay) ',
            '@day31' => '.flatpickr-day[aria-label*=" 31, "]:not(nextMonthDay):not(prevMonthDay) ',
        ];
    }

    /**
     * Select the given date.
     *
     * @param  \Laravel\Dusk\Browser  $browser
     * @param  int  $year
     * @param  int  $month
     * @param  int  $day
     * @return void
     */
    public function selectDate($browser, $year, $month, $day)
    {
        $browser->select('@monthSelect', $month - 1)
            ->type('@yearInput', $year)
            ->click('@day' . $day);
    }
}
