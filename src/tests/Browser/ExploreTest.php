<?php

namespace Tests\Browser;

use App\Project;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\ExplorePage;
use Tests\DuskTestCase;

class ExploreTest extends DuskTestCase
{
    use DatabaseMigrations;


    /**
     * Check if the page exists and is reachable
     * @throws Throwable
     */
    public function testExplorePageExists()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                    ->assertSee('Broken Feature')
                    ->clickLink('Explore')
                    ->on(new ExplorePage());
        });
    }


    /**
     * Check if we can reach a project page from the explore page
     * @throws Throwable
     */
    public function testProjectNavigation()
    {
        $user = factory(User::class)->create();
        // Create a project
        $project = factory(Project::class)->create([
            'voters_count' => 10,
            'upvotes_count' => 5,
            'updated_at' => Carbon::create(2020, 11, 20),
            'user_id' => $user->id,
        ]);

        $this->browse(function (Browser $browser) use ($project) {
            $browser->visit('/explore')
                ->on(new ExplorePage());
            $browser->assertSee('See project')
                ->clickLink('See project')
                ->assertTitleContains($project->name);
        });
    }


    /**
     * Check if we can reach a profile page from the explore page
     * @throws Throwable
     */
    public function testProjectCreatorNavigation()
    {
        $user = factory(User::class)->create();
        // Create a project
        $project = factory(Project::class)->create([
            'voters_count' => 10,
            'upvotes_count' => 5,
            'updated_at' => Carbon::create(2020, 11, 20),
            'user_id' => $user->id,
        ]);

        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/explore')
                ->on(new ExplorePage())
                ->assertSee($user->displayName)
                ->clickLink($user->displayName);
        });
    }

}
