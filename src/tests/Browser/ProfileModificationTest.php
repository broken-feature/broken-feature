<?php

namespace Tests\Browser;

use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\MyProfilePage;
use Tests\Browser\Pages\ProfileModificationPage;
use Tests\DuskTestCase;

class ProfileModificationTest extends DuskTestCase
{

    use DatabaseMigrations;

    public function testPrefilledFields()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();

            //login
            $browser->loginAs($user->id)
                ->visit('/profile')
                ->on(new MyProfilePage)
                ->click('@editProfileButton')
                ->on(new ProfileModificationPage)
                ->assertInputValue('@usernameInput', $user->username)
                ->assertInputValue('@displayNameInput', $user->displayName)
                ->assertInputValue('@emailInput', $user->email)
                ->assertInputValue('@websiteInput', $user->website)
                ->assertInputValue('@bioInput', $user->bio);
        });
    }

    public function testUpdateInfos()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();

            //login
            $browser->loginAs($user->id)
                ->visit('/profile/edit')
                ->on(new ProfileModificationPage);
            $updatedUsername = "udtUsername";
            $updatedDisplayName = "udpDisplayName";
            $updatedEmail = "udt@email.com";
            $updatedWebsite = "udtWebsite.com";
            $updatedBio = "updated bio";
            ProfileModificationPage::fillForm($browser, $updatedUsername, $updatedDisplayName, $updatedEmail, $updatedWebsite, $updatedBio);
            $browser->click('@submitButton')
                ->on(new MyProfilePage)
                ->assertSee("Your account has been successfully updated")
                ->assertSee($updatedUsername)
                ->assertSee($updatedDisplayName)
                ->assertSee($updatedEmail)
                ->assertSee($updatedWebsite)
                ->assertSee($updatedWebsite);
        });
    }

    public function testFillFormWithBadInputs()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();

            //login
            $browser->loginAs($user->id)
                ->visit('/profile/edit')
                ->on(new ProfileModificationPage);

            //Right inputs
            $updatedUsername = "udtUsername";
            $updatedDisplayName = "udpDisplayName";
            $updatedEmail = "udt@email.com";
            $updatedWebsite = "udtWebsite.com";
            $updatedBio = "updated bio";

            //Wrong inputs
            $wrongUpdatedUsername = "udtUs£rn@me";
            $wrongUpdatedEmail = "udtemailcom";

            //Wrong username
            ProfileModificationPage::fillForm($browser, $wrongUpdatedUsername, $updatedDisplayName, $updatedEmail, $updatedWebsite, $updatedBio);
            $browser->click('@submitButton')
                ->on(new ProfileModificationPage)
                ->assertSee("The username format is invalid.");

            //Wrong email
            ProfileModificationPage::fillForm($browser, $updatedUsername, $updatedDisplayName, $wrongUpdatedEmail, $updatedWebsite, $updatedBio);
            $browser->click('@submitButton')
                ->on(new ProfileModificationPage)
                ->assertSee("The email must be a valid email address.");

        });
    }
}
