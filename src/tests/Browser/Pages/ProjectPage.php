<?php

namespace Tests\Browser\Pages;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;

class ProjectPage extends Page
{

    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {

        $browser->assertVisible('@userSlug');
        $browser->assertVisible('@projectName');
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@userName' => '.bg-indigo-darker h3 a[href*="/u/"]',
            '@userSlug' => '.bg-indigo-darker a + span',
            '@projectName' => '.bg-indigo-darker h1',
            '@website' => '.bg-indigo-darker p a[target="_blank"]',
            '@bio' => '.bg-indigo-darker p>p',
            '@releaseDate' => '.bg-indigo-darker p>p>p.subtitle',
            '@followButton' => '#followForm .btn',
            '@followersCount' => '.followersCount',
        ];
    }

}
