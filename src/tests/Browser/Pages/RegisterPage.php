<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;

class RegisterPage extends Page
{
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/register';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url())
                ->assertTitle('Register - BrokenFeature')
                ->assertSee('Register');
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@emailInput' => 'input[name="email"]',
            '@usernameInput' => 'input[name="username"]',
            '@displayNameInput' => 'input[name="displayName"]',
            '@passwordInput' => 'input[name="password"]',
            '@confirmPasswordInput' => 'input[name="password_confirmation"]',
            '@submitButton' => '.btn',

        ];
    }
}
