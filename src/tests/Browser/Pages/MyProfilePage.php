<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;

class MyProfilePage extends Page
{
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/profile';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url())
            ->assertTitle('My profile - BrokenFeature')
            ->assertPresent('@editProfileButton')
            ->assertPresent('@newProjectButton')
            ->assertSee('My projects')
            ->assertSee('Followed projects');
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            //'@newProjectButton' => '#selector',
        ];
    }
}
