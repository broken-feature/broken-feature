<?php

namespace Tests\Browser\Pages;

use App\Project;
use App\User;
use Carbon\Carbon;
use Laravel\Dusk\Browser;

class SearchResultsPage extends Page
{
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/search';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathBeginsWith($this->url());
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@resultsAmount' => 'h1',
            '@resultsDiv' => 'h1 + .container',
        ];
    }
}
