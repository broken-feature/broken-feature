<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;
use Tests\Browser\Components\DatePicker;

class CreateProjectPage extends Page
{
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/projects/create';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url());
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@nameInput' => 'input[name="name"]',
            '@detailInput' => 'textarea[name="detail"]',
            '@datePicker' => 'input[name="releaseDate"]',
            '@websiteInput' => 'input[name="website"]',
            '@submit' => 'button[type="submit"]',
        ];
    }

    public static function createProject(Browser $browser, string $name, string $detail, string $website, string $year, int $month, int $day){
        $browser->type('@nameInput', $name)
            ->type('@detailInput', $detail)
            ->type('@websiteInput', $website)
            ->click('@datePicker')
            ->within(new DatePicker(), function($browser) use ($month, $year, $day) {
                $browser->selectDate($year, $month, $day);
            })
            ->click('@submit');
    }
}
